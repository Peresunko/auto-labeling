import csv

# Функция сохранения данных в файл csv
# UPD: добавил сортировку по номеру кадра и ID, потому что порядок мог быть иной в результате действий пользователя
def save_file(itemList, path):
  #itemList = sorted(itemList, key=lambda row: (row['frameNumber'],
  #                                             row['id']))
              
  with open(path, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    # Выводим заглавную строку
    writer.writerow([
      'Frame number',
      'id',
      'class', 
      'x', 
      'y', 
      'w', 
      'h', 
      'Confidence',
    ])
    # Выводим информацию о каждом объекте
    for item in itemList:
      writer.writerow([
        item['frameNumber'],
        item['id'], 
        item['class'],  
        item['x'], 
        item['y'], 
        item['w'], 
        item['h'], 
        item['confidence'],
      ])