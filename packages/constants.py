from cv2 import cv2 as cv

TRACKER_CSRT = 'csrt'
TRACKER_KCF = 'kcf'
TRACKER_BOOSTING = 'boosting'
TRACKER_MIL = 'mil'
TRACKER_TLD = 'tld'
TRACKER_MEDIANFLOW = 'medianflow'
TRACKER_MOSSE = 'mosse'

# Трекеры
Trackers = {
  TRACKER_CSRT: cv.legacy.TrackerCSRT_create,
  TRACKER_KCF: cv.legacy.TrackerKCF_create,
  TRACKER_BOOSTING: cv.legacy.TrackerBoosting_create,
  TRACKER_MIL: cv.legacy.TrackerMIL_create,
  TRACKER_TLD: cv.legacy.TrackerTLD_create,
  TRACKER_MEDIANFLOW: cv.legacy.TrackerMedianFlow_create,
  TRACKER_MOSSE: cv.legacy.TrackerMOSSE_create
}

# Адрес файла для сохранения данных
OUTPUT_FILE = r'./data/data.csv'
# Адрес файла для чтения
INPUT_FILE = r'./data/football.mov'
INPUT_FILE_STREET = r'./data/street.mov'
INPUT_FILE_WALKING360 = r'./data/peoplewalking360.mp4'
INPUT_FILE_WALKING720 = r'./data/peoplewalking720.mp4'
INPUT_FILE_WALKS_IN = r'./data/test.mp4'

# Данные о программе
ORGANIZATION_NAME = 'СФУ'
ORGANIZATION_DOMAIN = 'sfu-kras.ru'

# Сообщения о результатах работы функций и значения по умолчанию
# GlobalSettingsManager
SETTINGS_VALUE_APPLICATION_NAME = 'Программа для автоматического лейбелинга видео'

# ProjectManager
DEFAULT_PROJECT_NAME = 'Безымянный'

MESSAGE_PROJECT_FOLDER_CREATED = "Папка проекта успешно создана"  
MESSAGE_DICT_KEYS_MATCH = "Ключи словарей совпали, данные проекта успешно считаны"

MESSAGE_ERROR_PROJECT_FOLDER_ALREADY_EXISTS = "Проект с таким именем уже существует!"
MESSAGE_ERROR_DICT_KEYS_DO_NOT_MATCH = "Ключи словаря проекта не совпадают с ключами словаря дефолтного проекта"


# Сообщения для статус бара окна
STATUS_BAR_MSG_DEFAULT = "Приложение готово к работе"
STATUS_BAR_MSG_CURRENTLY_TRACKING = "Идет отслеживание объектов"
STATUS_BAR_MSG_CURRENTLY_SEARCHING_OBJECTS_ON_FRAME = "Нейронная сеть ищет объекты на кадре, подождите"
STATUS_BAR_MSG_CURRENTLY_DETECTING = "Идет распознавание объектов"
STATUS_BAR_MSG_CURRENTLY_SAVING_DATASET = "Идет сохранение датасета"
STATUS_BAR_MSG_DATASET_SAVED = "Датасет успешно сохранен: {0}"
STATUS_BAR_MSG_CURRENTLY_PLAYING = "Идет просмотр видео"