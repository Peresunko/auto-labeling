from PyQt5.QtWidgets import QApplication, QDialog, QDialogButtonBox, QFileDialog, QMessageBox, QPushButton
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5 import QtCore, QtGui

from packages.model.video_reader import VideoReader
from packages.model.combined import CombinedDetector
from packages.model.workers import OpenCVWorker, PlayerWorker, YOLOWorker
from packages.save_file import save_file

from packages.views.new_project_dialog import NewProjectDialog
from packages.views.settings_dialog import SettingsDialog

import packages.constants as const
import sys
import os


# В контроллере дела делаться будут те шо связаны с взаимодействием между детекторами нашими и интерфейсом
# Старался тут функции делать только те, которые связаны с взаимодействием, надеюсь, что нигде лишнего не впихнул
class WorkspaceController():
    # Не помню, зачем создавал, не используется, но пусть будет, вдруг вспомню))0
    changedRunningState = pyqtSignal(bool)


    def __init__(self, viewStartUp, viewWorkspace, projectManager, globalSettingsManager):
        # Для обнаружения и отслеживания
        self._model = None
        self._thread = None
        self._threadWorker = None

        # Константы
        self.THREAD_TASK_OPENCV = 0
        self.THREAD_TASK_YOLO = 1
        self.THREAD_TASK_PLAY = 2

        # Для видео
        self._currentFrame = 0
        self._currentlyRunning = False
        self._videoReader = VideoReader()

        # Менеджеры всякие
        self._projectManager = projectManager
        self._globalSettingsManager = globalSettingsManager

        # Окна
        self._viewStartUp = viewStartUp
        self._viewWorkspace = viewWorkspace
        #self._dialogNewProject = dialogNewProject
        #self._dialogSettings = dialogSettings

        # Сигналы и запуск
        self._setDefaultValues()
        self._connectAllSignals()

        recentProjects = self._globalSettingsManager.getSettingsValue(self._globalSettingsManager.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS)
        print(recentProjects)
        self._viewStartUp.setRecentProjects(recentProjects)
        self._viewStartUp.show()


    # Установка дефолтных значений для контроллера (при возвращении на первое окно ввода параметров)
    # TODO: если надо, то написать код для выставления каких-то дефолтных значений
    def _setDefaultValues(self):
        #self._globalSettingsManager.setDefaultSettings()

        #self._model = None
        #self._thread = None
        #self._threadWorker = None
        #self._projectData = None
        #self._currentFrame = 0
        #self._currentlyRunning = False
        #self._videoReader.releaseCapture()

        pass


    
    def _openProjectFromPath(self, path):
        res, message = self._projectManager.readProjectData(path)

        if res:
            projectName = self._projectManager.getProjectName()
            tracker = self._projectManager.getTrackerType()
            confidence = self._projectManager.getConfidenceThreshold()
            nms = self._projectManager.getNMSThreshold()
            
            self._model = CombinedDetector(confidence, nms, tracker)
            self._globalSettingsManager.updateListOfRecentProjects(self._projectManager.getProjectFilePath())
            self._viewWorkspace.setDefaultValues(projectName, confidence, nms, tracker)
            self._viewStartUp.hide()
            self._viewWorkspace.show()
        else:
            QMessageBox.warning(self._viewStartUp, "Ошибка чтения файла проекта", message)


    def _connectAllSignals(self):
        self._connectStartUpWindowSignals()
        #self._connectNewProjectDialogSignals()
        self._connectWorkspaceWindowSignals()
        #self._connectSettingsDialogSignals()



                                                            # Слоты для StartUpWindow

    # Функция соединения слотов и сигналов для начального окна
    def _connectStartUpWindowSignals(self):
        self._viewStartUp.ui.pushBtnNewProject.clicked.connect(lambda: self._createNewProjectFromStartUpWindow())
        self._viewStartUp.ui.pushBtnOpenProject.clicked.connect(lambda: self._openExistingProjectFromStartUpWindow())

        for button in self._viewStartUp.ui.groupBoxRecentFiles.findChildren(QPushButton):
            button.clicked.connect(lambda: self._openRecentProjectFromStartUpWindow())


    # Слот для нажатия кнопки создания нового проекта
    def _createNewProjectFromStartUpWindow(self):
        dialogNewProject = NewProjectDialog(self._viewStartUp)
        self._connectNewProjectDialogSignals(dialogNewProject, self._viewStartUp)
        defaultProjectFolder = self._globalSettingsManager.getSettingsValue(self._globalSettingsManager.SETTINGS_KEY_DEFAULT_PROJECTS_FOLDER)
        defaultProjectName = const.DEFAULT_PROJECT_NAME
        createSubfolder = self._globalSettingsManager.getSettingsValue(self._globalSettingsManager.SETTINGS_KEY_CREATE_SUBFOLDER_FOR_NEW_PROJECTS)

        dialogNewProject.setDefaultValues(defaultProjectName, defaultProjectFolder, createSubfolder)
        
        dialogNewProject.exec_()

        dialogNewProject.deleteLater()


    # Слот для открытия существующего проекта
    # TODO: дефолтные значения окна проекта надо проверить, чтоб все нормально работало при открытии других проектов
    def _openExistingProjectFromStartUpWindow(self):
        path = QFileDialog.getOpenFileName(self._viewStartUp, 'Выберите файл проекта', '', 'Проект разметки (*.lbproj)')
        
        if path[0]:
            self._openProjectFromPath(path[0])
        
        # Это только при нажатии на отмену вроде должно быть
        else:
            pass


    # Слот для открытия существуюещго проекта по нажатию на кнопку
    def _openRecentProjectFromStartUpWindow(self):
        target = self._viewStartUp.sender()
        path = target.toolTip()
        self._openProjectFromPath(path)



                                                            # Слоты для NewProjectDialog
    # Функция соединения слотов и сигналов
    # dialog - создаваемый диалог, для которого нужно соединять
    # parent - родительский виджет, на случай, если в зависимости от родителя
    #          нужны разные слоты
    def _connectNewProjectDialogSignals(self, dialog, parent):
        if parent is self._viewStartUp:
            dialog.ui.buttonBox.accepted.connect(lambda: self._newProjectDialogStartupAccept(dialog))
            dialog.ui.buttonBox.rejected.connect(lambda: self._newProjectDialogStartupReject(dialog))
        elif parent is self._viewWorkspace:
            dialog.ui.buttonBox.accepted.connect(lambda: self._newProjectDialogWorkspaceAccept(dialog))
            dialog.ui.buttonBox.rejected.connect(lambda: self._newProjectDialogWorkspaceReject(dialog))

        dialog.ui.lineEditProjectName.textChanged.connect(lambda: self._slotValidateNewProjectFolder(dialog))
        dialog.ui.lineEditProjectFolder.textChanged.connect(lambda: self._slotValidateNewProjectFolder(dialog))
        dialog.ui.checkBoxCreateSubfolder.stateChanged.connect(lambda: self._slotValidateNewProjectFolder(dialog))


    # Слот для события нажатия "ОК", диалог открывается от начального окна
    def _newProjectDialogStartupAccept(self, dialog):
        projectFolder = dialog.getNewProjectFolder()
        projectName = dialog.getNewProjectName()
        createSubfolder = dialog.getCreateSubfolderCheckBoxValue()

        res, msg = self._projectManager.createNewProject(projectName, projectFolder, createSubfolder)

        if res:
            QMessageBox.information(dialog, "Информация", msg)

            nmsThreshold = self._projectManager.getNMSThreshold()
            confidenceThreshold = self._projectManager.getConfidenceThreshold()
            tracker = self._projectManager.getTrackerType()

            self._model = CombinedDetector(confidenceThreshold, nmsThreshold, tracker)
            self._globalSettingsManager.updateListOfRecentProjects(self._projectManager.getProjectFilePath())
            self._viewWorkspace.setDefaultValues(projectName=self._projectManager.getProjectName())
            self._viewWorkspace.setClassNameList(self._model.getClassNameList())
            self._viewStartUp.hide()
            self._viewWorkspace.show()
            
        else:
            QMessageBox.warning(dialog, "Ошибка", msg)
            


    # Слот для события нажатия "Отмена", диалог открывается от начального окна
    # По сути здесь ничего и не надо, но на всякий случай пусть останется
    def _newProjectDialogStartupReject(self, dialog):
        pass


    # Слот для события нажатия "ОК", диалог открывается от окна работы с проектом
    def _newProjectDialogWorkspaceAccept(self, dialog):
        pass
            

    # Слот для события нажатия "Отмена", диалог открывается от окна работы с проектом
    # По сути здесь ничего и не надо, но на всякий случай пусть останется
    def _newProjectDialogWorkspaceReject(self, dialog):
        pass


    # Слот для события изменения пути нового проекта
    # Чтобы вместо выдавания ошибки просто отключалась кнопка ОК при некорректном пути, с подсказкой
    def _slotValidateNewProjectFolder(self, dialog):
        projectFolder = dialog.getNewProjectFolder()
        projectName = dialog.getNewProjectName()
        createSubfolder = dialog.getCreateSubfolderCheckBoxValue()

        res, tip = self._projectManager.isValidProjectFolder(projectFolder, projectName, createSubfolder)

        if res:
            dialog.ui.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        else:
            dialog.ui.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
            
        dialog.ui.buttonBox.button(QDialogButtonBox.Ok).setToolTip(tip)


                                                            # Слоты для WorkspaceWindow
    def _connectWorkspaceWindowSignals(self):
        # Menubar
        # Действия "Файл"
        self._viewWorkspace.ui.actionNewProject.triggered.connect(lambda: self._slotActionNewProject())
        self._viewWorkspace.ui.actionOpenProject.triggered.connect(lambda: self._slotActionOpenProject())
        #self._viewWorkspace.ui.actionOpenRecentProject.triggered.connect(lambda: self._slotActionNewProject())
        self._viewWorkspace.ui.actionOpenVideo.triggered.connect(lambda: self._slotActionOpenVideo())
        self._viewWorkspace.ui.actionSaveProject.triggered.connect(lambda: self._slotActionSaveProject())
        self._viewWorkspace.ui.actionSaveDataset.triggered.connect(lambda: self._slotActionSaveDataset())
        self._viewWorkspace.ui.actionSaveDatasetAs.triggered.connect(lambda: self._slotActionSaveDatasetAs())
        self._viewWorkspace.ui.actionExit.triggered.connect(lambda: self._slotActionExit())

        # Действия "Правка"
        self._viewWorkspace.ui.actionSettings.triggered.connect(lambda: self._slotActionSettings())

        # Действия "Справка"
        self._viewWorkspace.ui.actionAbout.triggered.connect(lambda: self._slotActionAbout())
        self._viewWorkspace.ui.actionRepositoryLink.triggered.connect(lambda: self._slotActionOpenRepository())


        # Рабочая область
        self._viewWorkspace.ui.pushBtnPlay.clicked.connect(lambda: self._slotPlayVideo())
        # Вкладка "Разметка"
        self._viewWorkspace.ui.pushBtnStartStopTracking.clicked.connect(lambda: self._slotStartStopTracking())
        self._viewWorkspace.ui.pushBtnFindFrameObjects.clicked.connect(lambda: self._slotFindObjectsOnFrame())
        self._viewWorkspace.ui.pushBtnStartStopSearch.clicked.connect(lambda: self._slotStartStopRecognition())
        self._viewWorkspace.ui.pushBtnSaveDataset.clicked.connect(lambda: self._slotSaveDataset())
        self._viewWorkspace.ui.pushBtnSaveSelectedObjectInfo.clicked.connect(lambda: self._updateModelCurrentFrameInfo())

        self._viewWorkspace.ui.pushBtnSaveSelectedObjectInfo.clicked.connect(self._slotSaveSelectedObjectInfo)
        self._viewWorkspace.ui.pushBtnDeleteSelectedObjectInfo.clicked.connect(self._slotDeleteSelectedObject)

        # Вкладка "Видео"
        self._viewWorkspace.ui.pushBtnOpenVideo.clicked.connect(lambda: self._slotOpenVideoFromPath())

        # Вкладка "Параметры детектора"
        self._viewWorkspace.ui.pushBtnApplyDetectorSettings.clicked.connect(lambda: self._slotApplyDetectorSettings())

        # Прочее 
        self._viewWorkspace.ui.hSliderVideo.valueChanged.connect(self._slotUpdateCurrentFrameOnSliderValueChanged)


                                # Действия MenuBar
                # Действия Файл

    def _slotActionNewProject(self):
        pass

    def _slotActionOpenProject(self):
        pass

    def _slotActionOpenRecentProject(self):
        pass

    def _slotActionOpenVideo(self):
        pass

    def _slotActionSaveProject(self):
        self._projectManager.writeProjectData()

    def _slotActionSaveDataset(self):
        self._slotSaveDataset()

    def _slotActionSaveDatasetAs(self):
        pass

    def _slotActionExit(self):
        pass

                # Действия Правка

    def _slotActionSettings(self):
        pass

                # Действия Справка

    def _slotActionAbout(self):
        pass

    def _slotActionOpenRepository(self):
        pass


                                # Вкладка Разметка


    # Слот для нажатия на кнопку проигрывания видео
    def _slotPlayVideo(self):
        if self._currentlyRunning:
            self._currentlyRunning = False
            self._viewWorkspace.ui.switchPlayButtonIcon(self._currentlyRunning)
            self._worker.stop()

            self._viewWorkspace.ui.pushBtnStartStopTracking(True)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(True)
            self._viewWorkspace.ui.pushBtnStartStopSearch.setEnabled(True)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(True)

            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_VIDEO_OPENED)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(True)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(True)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(True)
        else:
            self._currentlyRunning = True
            self._updateModelCurrentFrameInfo()
            self._viewWorkspace.ui.switchPlayButtonIcon(self._currentlyRunning)

            self._viewWorkspace.ui.pushBtnStartStopTracking.setEnabled(False)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(False)
            self._viewWorkspace.ui.pushBtnStartStopSearch.setEnabled(False)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(False)

            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_MULTITHREADING)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(False)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(False)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(False)

            targetClasses = self._viewWorkspace.getClassFilterList()
            self._thread, self._worker = self._createThread(task=self.THREAD_TASK_PLAY, targetClasses=targetClasses)
            self._thread.start()


    # Слот для кнопки остановки/возобновления процесса отслеживания объектов на видео
    # Останавливает/возобновляет работу потока по нажатию кнопки
    def _slotStartStopTracking(self):   
        if self._currentlyRunning:
            self._currentlyRunning = False
            self._switchSignals()
            self._worker.stop()

            self._viewWorkspace.renameStartStopButtonOpenCV(self._currentlyRunning)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(True)
            self._viewWorkspace.ui.pushBtnStartStopSearch.setEnabled(True)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(True)

            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_VIDEO_OPENED)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(True)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(True)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(True)
        else:
            self._currentlyRunning = True
            self._updateModelCurrentFrameInfo()
            self._switchSignals()

            self._viewWorkspace.renameStartStopButtonOpenCV(self._currentlyRunning)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(False)
            self._viewWorkspace.ui.pushBtnStartStopSearch.setEnabled(False)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(False)

            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_MULTITHREADING)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(False)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(False)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(False)

            targetClasses = self._viewWorkspace.getClassFilterList()
            self._thread, self._worker = self._createThread(task=self.THREAD_TASK_OPENCV, targetClasses=targetClasses)
            self._thread.start()

    
    # Слот для кнопки, которая есть только при выборе Комбинированного детектора
    # Запускает поток с работником ЙОЛО, чтобы на текущем кадре найти объекты
    def _slotFindObjectsOnFrame(self):
        targetClasses = self._viewWorkspace.getClassFilterList()
        self._thread, self._worker = self._createThread(task=self.THREAD_TASK_YOLO, runOnce=True, targetClasses=targetClasses)
        self._thread.start()
        #self._thread.wait()


    # Слот для кнопки остановки/возобновления процесса распознавания объектов на видео
    # Останавливает/возобновляет работу потока по нажатию кнопки
    def _slotStartStopRecognition(self):   
        if self._currentlyRunning:
            self._currentlyRunning = False
            self._switchSignals()
            self._worker.stop()

            self._viewWorkspace.ui.pushBtnStartStopTracking.setEnabled(True)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(True)
            self._viewWorkspace.renameStartStopButtonYOLO(self._currentlyRunning)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(True)
            
            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_VIDEO_OPENED)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(True)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(True)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(True)
        else:
            self._currentlyRunning = True
            self._updateModelCurrentFrameInfo()
            self._switchSignals()

            self._viewWorkspace.ui.pushBtnStartStopTracking.setEnabled(False)
            self._viewWorkspace.ui.pushBtnFindFrameObjects.setEnabled(False)
            self._viewWorkspace.renameStartStopButtonYOLO(self._currentlyRunning)
            self._viewWorkspace.ui.pushBtnSaveDataset.setEnabled(False)

            self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_MULTITHREADING)
            #self._viewWorkspace.ui.manualObjectDetection.setEnabled(False)
            #self._viewWorkspace.ui.YOLOFilter.setEnabled(False)
            #self._viewWorkspace.ui.frameVideoControls.setEnabled(False)

            targetClasses = self._viewWorkspace.getClassFilterList()
            self._thread, self._worker = self._createThread(task=self.THREAD_TASK_YOLO, runOnce=False, targetClasses=targetClasses)
            self._thread.start()


    # Слот для кнопки сохранения датасета из панели управления
    def _slotSaveDataset(self):
        videoName = self._videoReader.getVideoPath()
        videoName = os.path.basename(videoName).split('.')[0]
        outputFile = os.path.join(self._projectManager.getProjectFolder(), 'datasets', "{0}.csv".format(videoName))
        save_file(self._model.getFramesInfoList(), outputFile)
        self._projectManager.addDatasetPath(outputFile)
        QMessageBox.information(self._viewWorkspace, "Датасет успешно сохранен!", "Путь до датасета:\n{0}".format(outputFile))


    # Слот для кнопки сохранения информации о выделенном объекте
    def _slotSaveSelectedObjectInfo(self):
        self._viewWorkspace.saveSelectedObjectInfo()
        self._updateModelCurrentFrameInfo()
        pass


    # Слот для удаления выбранного объекта
    def _slotDeleteSelectedObject(self):
        self._viewWorkspace.deleteSelectedObject()
        self._updateModelCurrentFrameInfo()
        pass

                                # Вкладка Видео


    def _slotOpenVideoFromPath(self):
        path = self._viewWorkspace.getVideoPath()

        self._currentFrame = 0
        self._videoReader.setSourceData(path)

        x, y = self._videoReader.getShape()
        frameCount = self._videoReader.getFrameCount()
        fps = self._videoReader.getFPS()

        self._viewWorkspace.switchStateOfUIElements(self._viewWorkspace.UI_STATE_VIDEO_OPENED)
        self._viewWorkspace.setPreferredFrameSize(x, y)
        self._viewWorkspace.setFPS(fps)
        self._viewWorkspace.ui.labelTotalTime.setText(str(frameCount))
        self._viewWorkspace.setMaximumSliderValue(frameCount)
        self._viewWorkspace.updateFrameData(self._videoReader.getFrame(next=False, i=self._currentFrame), i=self._currentFrame, frameInfo=[])
        self._viewWorkspace.adjustSize()

        self._projectManager.addVideoPath(self._videoReader.getVideoPath())

        self._model.clearFrameInfoList()


    def _slotLoadDatasetFromPath(self):
        #path = self._viewWorkspace.getDatasetPath()
        pass

    def _slotApplyPrefferedFrameSize(self):
        res, payload = self._viewWorkspace.getPreferredFrameSize()

        if res:
            self._model.clearFrameInfoList()
            self._videoReader.setScreenSize(payload)
            self._viewWorkspace.updateFrameData(self._videoReader.getFrame(next=False, i=self._currentFrame), i=self._currentFrame, frameInfo=[])

        else:
            QMessageBox.warning(self._viewWorkspace, "Ошибка изменения размера кадра", payload)


                                # Вкладка Параметры детектора

    def _slotApplyDetectorSettings(self):
        tracker = self._viewWorkspace.getSelectedTrackerType()
        confidence = self._viewWorkspace.getConfidenceThreshold()
        nms = self._viewWorkspace.getNMSThreshold()

        self._projectManager.setTrackerType(tracker)
        self._projectManager.setConfidenceThreshold(confidence)
        self._projectManager.setNMSThreshold(nms)

        self._model.setTrackerType(tracker)
        self._model.setConfidenceThreshold(confidence)
        self._model.setNMSThreshold(nms)


                                # Прочие слоты окна работы с проектом


    # Слот для события изменения значения слайдера - меняет кадр, запрашивая по нему всю инфу, обновляет другие элементы интерфейса
    def _slotUpdateCurrentFrameOnSliderValueChanged(self):
        self._currentFrame = self._viewWorkspace.getSliderValue()
        self._viewWorkspace.changeWorkMode(target=self._viewWorkspace.SENDER_BTN_VIEW_MODE)
        frameInfo = self._model.getFrameInfo(self._currentFrame)
        self._viewWorkspace.updateFrameData(self._videoReader.getFrame(next=False, i=self._currentFrame), i=self._currentFrame, frameInfo=frameInfo)

    
    # Аналогично функции выше - но тут для обработки сигнала из потока progress
    # Тут данные об коробках так же от потока приходят в сигнале
    def _slotUpdateCurrentFrameFromThread(self, ret, frame, i, frameInfo):
        self._currentFrame = i
        self._viewWorkspace.updateFrameData((ret, frame), i, frameInfo)


    


    # Слот для кнопки сохранения данных о кадре
    # Если кнопку эту не нажать, то изменения, сделанные на текущем кадре, не будут сохранены в детекторе
    def _updateModelCurrentFrameInfo(self):
        frameInfo = self._viewWorkspace.ui.labelVideoFrame.getCurrentFrameInfo()
        self._model.setFrameInfo(self._currentFrame, frameInfo)


                                # Приватные функции для окна работы с проектом


    # Функция для привязывания/отвязывания слота обновления кадра от сигнала изменения слайдера
    # Кадры видео могут меняться двумя способами - руками пользователя (передвижение слайдера) или работой потока
    # (передача кадра и его информации в функцию). Во время работы потока значение слайдера автоматически меняется,
    # но это все равно тригерит событие изменения значения. Поэтому отключаем его во время работы поотока
    def _switchSignals(self):
        if self._currentlyRunning:
            self._viewWorkspace.ui.hSliderVideo.valueChanged.disconnect(self._slotUpdateCurrentFrameOnSliderValueChanged)
        else:
            self._viewWorkspace.ui.hSliderVideo.valueChanged.connect(self._slotUpdateCurrentFrameOnSliderValueChanged)


    # Функция создания потока и нужного работника
    def _createThread(self, task, runOnce=False, targetClasses=[]):
        thread = QThread()

        if task is self.THREAD_TASK_OPENCV:
            worker = OpenCVWorker(self)
        elif task is self.THREAD_TASK_YOLO:
            worker = YOLOWorker(self)
            worker.setTargetClasses(targetClasses)
            worker.setRunOnce(runOnce)
        elif task is self.THREAD_TASK_PLAY:
            worker = PlayerWorker(self)
            worker.setFPS(self._videoReader.getFPS())

        worker.moveToThread(thread)
        thread.started.connect(worker.run)
        worker.progress.connect(self._slotUpdateCurrentFrameFromThread)
        worker.finished.connect(thread.quit)
        worker.finished.connect(worker.deleteLater)
        thread.finished.connect(thread.deleteLater)
        return thread, worker


                                                            # Слоты для SettingsDialog
    def _connectSettingsDialogSignals(self):
        pass
    
    
    def _settingsDialogAccept(self):
        pass


    def _settingsDialogReject(self):
        pass

