# Файл с названиями классов объектов
CLASSES_FILE = r'./packages/yolo/coco.names'
# Файл с конфигурацией модели YOLOv3
MODEL_CONFIG_FILE = r'./packages/yolo/yolov3.cfg'
# Файл с весами модели YOLOv3
MODEL_WEIGHTS_FILE = r'./packages/yolo/yolov3.weights'
# Ширина и высота изображения на входе в модель
TARGET_WIDTH_HEIGHT = 608