from cv2 import cv2 as cv


# Класс для чтения видеофайла
class VideoReader():
    def __init__(self):
        self._cap = None
        self._path = None
        self._shape = None
        self._fps = None
        self._frameCount = None
        self._screenSize = None
        self._lastGoodFrame = None


    # Функция установки пути к файлу
    def setSourceData(self, path, screenSize=None):
        self._path = path
        self._cap = cv.VideoCapture(path)
        width = self._cap.get(cv.CAP_PROP_FRAME_WIDTH)
        height = self._cap.get(cv.CAP_PROP_FRAME_HEIGHT)
        self._shape = (width, height)
        self._fps = self._cap.get(cv.CAP_PROP_FPS)
        self._frameCount = int(self._cap.get(cv.CAP_PROP_FRAME_COUNT))
        #self._frameCount = self.countFramesManually() - 1
        self._screenSize = screenSize
        print("FPS:", self._fps, "\nFrameCount:", self._frameCount)


    # Функция для получения кадра из видео
    def getFrame(self, next=True, i=0):
        if not next:
            self._cap.set(cv.CAP_PROP_POS_FRAMES, i)
        
        ret, frame = self._cap.read()

        if ret:
            if self._screenSize is not None:
                print("Yes, it's supossed to resize")
                frame = cv.resize(frame, self._screenSize)
            self._lastGoodFrame = frame
            return ret, frame
        else:
            print("Uh oh, bad frame!")
            return ret, self._lastGoodFrame


    # Для получения размерностей кадра
    # Наверна было надо когда-то, но не используется
    def getShape(self):
        return self._shape


    def setScreenSize(self, screenSize):
        self._screenSize = screenSize

    def getVideoPath(self):
        return self._path

    def getFPS(self):
        return self._fps

    # Для получения количества кадров в видео
    def getFrameCount(self):
        return self._frameCount

    
    # I hate myself
    # OpenCV не всегда правильно читает количество кадров, в итоге последние несколько "кадров" просто не существуют
    # Это альтернативный вариант - пройтись по всему видео, вручную посчитать кадры и использовать это значение
    def countFramesManually(self):
        i = 0

        while True:
            self._cap.set(cv.CAP_PROP_POS_FRAMES, i)
            ret, frame = self._cap.read()

            if not ret:
                break

            i += 1
        
        return i


    # Закрываем поток видоса
    def releaseCapture(self):
        self._cap.release()
        self._shape = None
        self._fps = None
        self._frameCount = None
        self._lastGoodFrame = None