from PyQt5.QtCore import QSettings

import packages.constants as const
import os

# Класс менеджера глобальных настроек приложения
# Для добавления нового глобального параметра:
#   - создать ключ настроек в виде "константы" SETTINGS_KEY_*
#   - добавить значение по умолчанию для нового ключа в self._defaultValues - для установки значения по умолчанию при первом запуске/сбросе
#   - добавить новый ключ в нужное место в self._valueTypes - для корректного чтения настроек из файла
#     если нужного типа там нет, скопировать установку типа и ключа и поменять на нужные значения
#     P.S. вещи вроде списка словарей/своих объектов в этом духе наверна не прокатит, вроде QSettings только с простыми работает
#   - если значение настройке нельзя задать просто через setSettingsValue(ключ, значение), то написать новую функцию (как у недавно открытых проектов)
class GlobalSettingsManager():

    def __init__(self):
        # Создаем объект настроек. Дает доступ к настройкам, связанным с нашим приложением
        # Приложение определяется по данным, указанным в main.py/main_wip.py у QCoreApplication
        self._settings = QSettings()

        # Константы для обращения к изменяемым настройкам
        # Ключи настроек
        self.SETTINGS_KEY_APPLICATION_NAME = "appInfo/appName"
        self.SETTINGS_KEY_DEFAULT_PROJECTS_FOLDER = "projectsSettings/defaultProjectsFolder"
        self.SETTINGS_KEY_CREATE_SUBFOLDER_FOR_NEW_PROJECTS = "projectsSettings/createProjectsFolder"
        self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS = "appInfo/recentlyOpenedProjects"
        self.SETTINGS_KEY_MAX_NUMBER_OF_RECENT_PROJECTS = "projectsSettings/maxNumberOfRecentProjects"

        # Значения по умолчанию
        self._defaultValues = {
            self.SETTINGS_KEY_APPLICATION_NAME:                     const.SETTINGS_VALUE_APPLICATION_NAME,
            self.SETTINGS_KEY_DEFAULT_PROJECTS_FOLDER:              os.path.expanduser('~\\{0}'.format(const.SETTINGS_VALUE_APPLICATION_NAME)),
            self.SETTINGS_KEY_CREATE_SUBFOLDER_FOR_NEW_PROJECTS:    True,
            self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS:             list(),
            self.SETTINGS_KEY_MAX_NUMBER_OF_RECENT_PROJECTS:        5
        }

        # Для перевода получаемых из getSettingsValue значений в нужный тип
        self._valueTypes = {
            'stringType': {},
            'boolType': {},
            'listType': {},
            'intType': {}
        }

        self._valueTypes['stringType']['type'] = str
        self._valueTypes['stringType']['keys'] = [
            self.SETTINGS_KEY_APPLICATION_NAME,
            self.SETTINGS_KEY_DEFAULT_PROJECTS_FOLDER
        ]

        self._valueTypes['boolType']['type'] = bool
        self._valueTypes['boolType']['keys'] = [
            self.SETTINGS_KEY_CREATE_SUBFOLDER_FOR_NEW_PROJECTS
        ]

        self._valueTypes['listType']['type'] = list
        self._valueTypes['listType']['keys'] = [
            self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS
        ]

        self._valueTypes['intType']['type'] = int
        self._valueTypes['intType']['keys'] = [
            self.SETTINGS_KEY_MAX_NUMBER_OF_RECENT_PROJECTS
        ]

        # При запуске приложения и создании объекта этого класса, мы проходим по настройкам и создаем недостающие
        self.setDefaultSettings(setToDefault=self._isFirstLaunch())

    
    # Функция проверки того, первый ли это запуск приложения на компьютере
    # По названию проверю просто потому шо эта настройка точно всегда будет после первого запуска
    def _isFirstLaunch(self):
        return not self._settings.contains(self.SETTINGS_KEY_APPLICATION_NAME)


    # Функция установки дефолтных настроек (при первом запуске все, при последующих - те, что пропущены, если, например, были новые добавлены)
    # setToDefault - для сброса всех настроек до значений по умолчанию (первый запуск/по нажатию кнопки)
    def setDefaultSettings(self, setToDefault):
        for key, value in self._defaultValues.items():
            if setToDefault or not self._settings.contains(key):
                self._settings.setValue(key, value)

        recentProjects = self.getSettingsValue(self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS)
        missingIdx = [idx for idx, project in enumerate(recentProjects) if not os.path.isfile(project)]

        for index in sorted(missingIdx, reverse=True):
            del recentProjects[index]

        self.setSettingsValue(self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS, recentProjects)

        self._settings.sync()


    # Функция получения значения настройки по ключу
    # ключи в основном коде передаем в виде обращений к константам экземпляра этого класса
    def getSettingsValue(self, key):
        requiredType = str
        
        for valueType in self._valueTypes:
            if key in self._valueTypes[valueType]['keys']:
                requiredType = self._valueTypes[valueType]['type']
                break

        return self._settings.value(key, type=requiredType)


    # Функция установки значений настроек,  
    def setSettingsValue(self, key, value):
        self._settings.setValue(key, value)
        self._settings.sync()


    # Функция для обновления списка недавно открытых проектов
    # тут не обойтись уже setSettingsValue, поэтому для этого отдельная функция
    def updateListOfRecentProjects(self, projectPath):
        recentProjects = self._settings.value(self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS, type=list)
        maxNumberOfRecent = self._settings.value(self.SETTINGS_KEY_MAX_NUMBER_OF_RECENT_PROJECTS, type=int)

        if projectPath in recentProjects:
            recentProjects.remove(projectPath)

        recentProjects.insert(0, projectPath)

        while len(recentProjects) > maxNumberOfRecent:
            recentProjects.pop()

        self._settings.setValue(self.SETTINGS_KEY_RECENTLY_OPENED_PROJECTS, recentProjects)
        self._settings.sync()
