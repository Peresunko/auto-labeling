import cv2 as cv
import numpy as np

from ..yolo.constants import CLASSES_FILE, MODEL_CONFIG_FILE, MODEL_WEIGHTS_FILE, TARGET_WIDTH_HEIGHT as twh
from ..constants import Trackers


# Класс на основе YOLO + OpenCV
class CombinedDetector():
    def __init__(self, confidenceThreshold, nmsThreshold, tracker):
        self._confidenceThreshold = confidenceThreshold
        self._nmsThreshold = nmsThreshold
        self._tracker = tracker
        self._multiTracker = None
        
        # Загружаем названия всех классов
        self._classNameList = []
        # Считываниям названия классов объектов из файла
        with open(CLASSES_FILE, 'rt') as f:
            self._classNameList = f.read().rstrip('\n').split('\n')

        # Считываем модель из Darknet (создаем модель), устанавливаем ей бэкенд OpenCV Darknet и запуск на CPU
        self._model = cv.dnn.readNetFromDarknet(MODEL_CONFIG_FILE, MODEL_WEIGHTS_FILE)
        #model.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
        self._model.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA)
        #model.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)
        self._model.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA)
        # Да, КУДА говорит, что не собрано под нее ничего, это надо гуглить уже. В итоге все на процессоре запускается

        # Создаем список для покадровой информации о видео
        self._framesInfoList = []

    # Функция для получения информации о классах YOLO
    def getClassNameList(self):
        return self._classNameList

    # Функция для получения от YOLO информации об объектах на текущем кадре
    def findObjects(self, image, frameNumber, targetClassList=[]):
        # Для фильтрации по классам
        isClassNeeded = True

        # Чистим данные по текущему кадру
        self.setFrameInfo(frameNumber, [])

        # Конвертируем изображение в blob и устанавливаем его в качестве входа
        blob = cv.dnn.blobFromImage(image, 1/255, (twh, twh), [0, 0, 0], 1 )
        self._model.setInput(blob)

        # Получаем названия выходных слоёв
        layerNameList = self._model.getLayerNames()
        outputNameList = [layerNameList[layer[0] -1] for layer in self._model.getUnconnectedOutLayers()]

        # Получаем выходы модели (данные с 3-х выходных слоёв)
        outputList = self._model.forward(outputNameList)

        # Получаем ширину и высоту полученного изображения
        imageHeight, imageWidth, imageChannels = image.shape
        # Создаем списки для сохранения результатов
        boundingBoxList = []
        classIdList = []
        confidenceList = []

        # Обрабатываем полученные выходы нейросети
        for output in outputList:
            for detection in output:
                # Получаем список уверенностей для каждого класса объектов, элементы с 5 по 84
                scores = detection[5:]
                # Находим индекс класса с максимальной уверенностью
                classId = np.argmax(scores)
                # Получаем уверенность для класса
                confidence = scores[classId]
                
                # Проверяем нужно ли распознавать этот класс
                if len(targetClassList) > 0:
                    # Получаем класс распознанного объекта
                    currentClassName = self._classNameList[classId]
                    # Если класс распознанного объекта отсутствует в списке целевых классов, то класс не нужен
                    if currentClassName not in targetClassList:
                        isClassNeeded = False
                    else:
                        isClassNeeded = True
                        
                else:
                    isClassNeeded = True

                # Обрабатываем detection (распознанный объект), если его уверенность выше порога
                if confidence > self._confidenceThreshold and isClassNeeded:

                    # Получаем ширину и длину рамки в пикселях
                    # В detection они идут в процентах
                    w, h = int(detection[2] * imageWidth), int(detection[3] * imageHeight)
                    # Получаем координаты верхнего левого угла рамки в пикселях
                    # В detection идут координаты центральной точки рамки
                    x, y = int(detection[0] * imageWidth - w / 2), int(detection[1] * imageHeight - h / 2)

                    # Сохраняем данные рамки, класс объекта и уверенность распознавания
                    boundingBoxList.append([x, y, w, h])
                    classIdList.append(classId)
                    confidenceList.append(float(confidence))

        # Получаем индексы рамок, которые нужно оставить, чтобы не было наложения
        indices = cv.dnn.NMSBoxes(boundingBoxList, confidenceList, self._confidenceThreshold, self._nmsThreshold)
        
        # Сохраняем данные оставляемых рамок
        detectedObjectList = []
        id = 0
        for i in indices:
            # Изббавляемся от излишней вложенности
            i = i[0]
            # Получаем данные объекта
            id = id + 1
            detectedObject = {
                'id': id,
                'class': self._classNameList[classIdList[i]],
                'boundingbox': boundingBoxList[i],
                'confidence': confidenceList[i],
            }
            # Сохраняем объект в список
            detectedObjectList.append(detectedObject)

        # Возвращаем результат работы функции (список распознанных объектов)
        for detectedObject in detectedObjectList:
            (x, y, w, h) = [int(a) for a in detectedObject['boundingbox']]

            self._framesInfoList.append({
                'frameNumber': frameNumber,
                'id': detectedObject['id'],
                'class': detectedObject['class'],
                'x': x,
                'y': y,
                'w': w,
                'h': h,
                'confidence': detectedObject['confidence'],
            })


    # Функция обработки кадра - берет картинку, номер кадра и то, первый ли это запуск функции
    # в ходе работы потока обработки
    def processFrame(self, image, frameNumber, recreateMultitrack):
        # Если это первый наш кадр обработки, то мы должны пересоздать мультитрак
        # с данными текущего кадра, которые могли быть (по идее и должны) обновлены пользователем
        # поэтому тут по сути только создаем мультитрак, а после этого забираем уже известные данные из этого класса
        if recreateMultitrack:
            # Утечка памяти? Надеюсь нет, где-то было что-то про это написано,
            # но скорее всего это про С++ было
            if self._multiTracker is not None:
                self._multiTracker = None
            self._multiTracker = cv.legacy.MultiTracker_create()

            frameObjects = self.getFrameInfo(frameNumber)

            for frameObject in frameObjects:
                boundingBox_i = (frameObject['x'], frameObject['y'], frameObject['w'], frameObject['h'])
                self._multiTracker.add(Trackers[self._tracker](), image, boundingBox_i)

        # Если кадр не первый, то сразу берем данные по новому кадру и записываем их
        # UPD: я не уверен, но возможно индексы объектов слетят, если, например, трекили 3 объекта (0, 1 и 2),
        # а потом на кадре вдруг не смог отследить объект 1, тогда 2 станет с индексом 1, вроде бы
        else:
            # Чистим данные по текущему кадру
            self.setFrameInfo(frameNumber, [])

            (success, boxes) = self._multiTracker.update(image)

            if success:
                frameInfo = self.getFrameInfo(frameNumber - 1)
                print('Frame Info:', frameInfo)
                for i in range(len(boxes)):
                    (x, y, w, h) = [int(a) for a in boxes[i]]
                    
                    print('I: ', i)
                    self._framesInfoList.append({
                        'frameNumber': frameNumber,
                        'id': i,
                        'class': frameInfo[i]['class'],
                        'x': x,
                        'y': y,
                        'w': w,
                        'h': h,
                        'confidence': frameInfo[i]['confidence'],
                    })


    # Для получения информации о текущем кадре - для отрисовки на интерфейсе
    def getFrameInfo(self, i):
        return [d for d in self._framesInfoList if d['frameNumber'] is i]

    # Для получения всего списка информации для сохранения
    def getFramesInfoList(self):
        return self._framesInfoList


    # Для изменения списка информации через интерфейс
    def setFrameInfo(self, i, frameInfo):
        oldInfoIdx = [idx for idx, element in enumerate(self._framesInfoList) if element['frameNumber'] is i]
        for index in sorted(oldInfoIdx, reverse=True):
            del self._framesInfoList[index]
        self._framesInfoList.extend(frameInfo)

    def clearFrameInfoList(self):
        self._framesInfoList.clear()


    def setNMSThreshold(self, nmsThreshold):
        self._nmsThreshold = nmsThreshold

    def setConfidenceThreshold(self, confidenceThreshold):
        self._confidenceThreshold = confidenceThreshold

    def setTrackerType(self, trackerType):
        self._tracker = trackerType
