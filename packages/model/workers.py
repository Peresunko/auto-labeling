from PyQt5.QtCore import QMutex, QObject, QTimer, pyqtSignal
import numpy as np


# Родитель всех рабочих
class Worker(QObject):
    # finished - при завершении работы (ручная остановка/конец видеозаписи)
    # progress - результаты работы по каждому кадру
    finished = pyqtSignal()
    progress = pyqtSignal(bool, np.ndarray, int, list)

    def __init__(self, parent=None):
        super().__init__(None)
        self._running = True
        self._mutex = QMutex()
        self._videoReader = parent._videoReader
        self._model = parent._model
        self._frameCount = self._videoReader.getFrameCount()
        self._currentFrame = parent._currentFrame

    # Основная работа потока тут
    def run(self):
        pass

    # Проверка того, работает ли работник
    def running(self):
        try:
            self._mutex.lock()
            return self._running
        finally:
            self._mutex.unlock()

    # Для остановки из главного потока
    def stop(self):
        self._mutex.lock()
        self._running = False
        self._mutex.unlock()



# Работник потока для YOLO части
class YOLOWorker(Worker):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._targetClasses = []
        self._runOnce = True

    # Основная работа потока тут
    def run(self):
        # Если комбинированный режим, то работник YOLO должен только один раз отработать - чтобы получить данные по текущем кадру
        if self._runOnce:
            ret, frame = self._videoReader.getFrame(False, self._currentFrame)

            self._model.findObjects(frame, self._currentFrame, self._targetClasses)
            frameInfo = self._model.getFrameInfo(self._currentFrame)

            self.progress.emit(ret, frame, self._currentFrame, frameInfo)

        # Иначе будет гнать по всем кадрам, пока не дойдет до конца/его не остановят
        elif not self._runOnce:
            while self._currentFrame < self._frameCount and self.running():
                ret, frame = self._videoReader.getFrame(False, self._currentFrame)

                self._model.findObjects(frame, self._currentFrame, self._targetClasses)
                frameInfo = self._model.getFrameInfo(self._currentFrame)

                self.progress.emit(ret, frame, self._currentFrame, frameInfo)
                self._currentFrame += 1
                
        self.finished.emit()

    def setTargetClasses(self, targetClassList):
        self._targetClasses = targetClassList

    def setRunOnce(self, runOnce):
        self._runOnce = runOnce


# Работник потока для OpenCV части
class OpenCVWorker(Worker):
    # Основная работа потока тут
    def run(self):
        # При запуске надо мультитрак сбросить и создать заново, чтобы изменения пользователя принять во внимание
        recreateMultitrack = True

        while self._currentFrame < self._frameCount and self.running():
            ret, frame = self._videoReader.getFrame(False, self._currentFrame)

            self._model.processFrame(frame, self._currentFrame, recreateMultitrack)
            frameInfo = self._model.getFrameInfo(self._currentFrame)
            
            self.progress.emit(ret, frame, self._currentFrame, frameInfo)

            self._currentFrame += 1
            recreateMultitrack = False
            
        self.finished.emit()


# Работник потока для обычного просмотра видео
class PlayerWorker(Worker):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._timer = QTimer(self)
        self._timer.timeout.connect(self._showFrame)
        self._fps = 30

    # Основная работа потока тут
    def run(self):
        self._timer.start(int(1000 / self._fps))

        while True:
            if not self._currentFrame < self._frameCount or not self.running():
                self._timer.stop()
                break
            
        self.finished.emit()

    # Функция для установки ФПС
    def setFPS(self, fps):
        self._fps = fps
    
    def _showFrame(self):
        ret, frame = self._videoReader.getFrame(False, self._currentFrame)
        frameInfo = self._model.getFrameInfo(self._currentFrame)
        self.progress.emit(ret, frame, self._currentFrame, frameInfo)
        self._currentFrame += 1    
