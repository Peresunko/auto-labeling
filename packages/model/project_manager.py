import packages.constants as const
import json
import os


# Класс менеджера проекта, ответственного за все общее, что связано с проектами пользователя
# Конкретно с файлом самого проекта и организацией папок, вещи вроде запись кадров видео по картинкам 
# и в будущем с нейронкой всякое лучше вынести в другой класс
class ProjectManager():

    # projectData - информация о проекте, которая сохраняется
    # tempData - любая информация, которая может быть нужна в ходе работы программы, но
    # которую не нужно сохранять (существуют ли открытые ранее видео, например)
    def __init__(self):
        self._projectData = None
        self._tempData = None
        self._projectSubfolders = [
            'frames',
            'datasets',
            'yolo'
        ]

        self.setDefaultValues()


    # Функция для сброса значений на дефолтные (при создании пользователем нового проекта) 
    # TODO: для локализации наверна надо вынести все строки в отдельный файл и оттуда обращаться, но пофег, пусть пока так))0
    def setDefaultValues(self):
        self._projectData = {}
        self._tempData = {}

        self.setProjectName(const.DEFAULT_PROJECT_NAME)
        self.setProjectFolder('')
        self.setVideoPaths()
        self.setDatasetPaths()
        self.setTrackerType()
        self.setConfidenceThreshold()
        self.setNMSThreshold()


    # Функция для создания нового проекта
    # TODO 1: Через QSettings задать имя программы нашей и оттуда брать/брать оттуда пользовательскую папку для проектов
    # TODO 2: Список папок для создания в папке проекта наверна тоже стоит в отдельное место вынести
    # EDIT 1: мне очень интересно, что я имел ввиду, когда писал "оттуда брать/брать оттуда" в todo 1)))0
    def createNewProject(self, projectName, projectFolder, createSubfolder):
        self.setDefaultValues()

        if createSubfolder:
            projectFolder = os.path.join(projectFolder, projectName)

        self.setProjectName(projectName)
        self.setProjectFolder(projectFolder)

        if createSubfolder and os.path.isdir(os.path.join(projectFolder, projectName)):
            return False, const.MESSAGE_ERROR_PROJECT_FOLDER_ALREADY_EXISTS
        else:
            if not os.path.isdir(projectFolder):
                os.makedirs(projectFolder)
            
            for subfolder in self._projectSubfolders:
                if not os.path.isdir(os.path.join(projectFolder, subfolder)):
                    os.mkdir(os.path.join(projectFolder, subfolder))

            self.writeProjectData()

            return True, const.MESSAGE_PROJECT_FOLDER_CREATED      


    # Функция для чтения и проверки данных проекта
    # path - указанный пользователем путь до файла проекта
    def readProjectData(self, path):
        self.setDefaultValues()

        with open(path) as projectFile:
            data = json.load(projectFile)
            data = json.loads(data)

            if data.keys() != self._projectData.keys():
                return False, const.MESSAGE_ERROR_DICT_KEYS_DO_NOT_MATCH

            self._projectData = data
            print(data)
            return True, const.MESSAGE_DICT_KEYS_MATCH


    # Функция для записи данных проекта
    def writeProjectData(self, path=None):
        if not path:
            path = os.path.join(self.getProjectFolder(), "{0}.lbproj".format(self.getProjectName()))
        
        with open(path, 'w') as projectFile:
            jsonStr = json.dumps(self._projectData)
            json.dump(jsonStr, projectFile, indent=4)


    # Валидация пути до проекта
    # TODO: надо бы ещё наверна проверку правильности пути
    def isValidProjectFolder(self, projectFolder, projectName, createSubfolder):
        projectFileName = "{0}.lbproj".format(projectName)
        
        if createSubfolder:
            fullPath = os.path.join(projectFolder, projectName, projectFileName)
        else:
            fullPath = os.path.join(projectFolder, projectFileName)
        
        if os.path.isfile(fullPath):
            return False, "Файл проекта с таким названием уже есть по указанному пути"
        else:
            return True, ""



                                            # Ниже всякие геты-сеты и прочее с projectData, чтобы не искать потом очепятку в названии ключа где-то
                                            # TODO: мб выделить _ те, которые только в пределах этого класса должны использоваться?

    # Функция получения пути до файла проекта
    def getProjectFilePath(self):
        return os.path.join(self.getProjectFolder(), "{0}.lbproj".format(self.getProjectName()))


    # Функция получения имени проекта
    def getProjectName(self):
        return self._projectData['projectName']
    # Функция изменения имени проекта
    def setProjectName(self, projectName):
        self._projectData['projectName'] = projectName


    # Функция получения пути до папки проекта
    def getProjectFolder(self):
        return self._projectData['projectFolder']
    # Функция изменения пути до папки проекта
    def setProjectFolder(self, projectFolder):
        self._projectData['projectFolder'] = projectFolder


    # Функция получения списка путей до видеофайлов
    def getVideoPaths(self):
        return self._projectData['videoPaths']
    # Функция изменения списка путей до видеофайлов
    def setVideoPaths(self, videoPaths=list()):
        self._projectData['videoPaths'] = videoPaths
    # Функция изменения пути до видеофайла
    def setVideoPath(self, i, videoPath):
        self._projectData['videoPaths'][i] = videoPath
    # Функция добавления пути к новому видеофайлу
    def addVideoPath(self, videoPath):
        if videoPath not in self._projectData['videoPaths']:
            self._projectData['videoPaths'].append(videoPath)
    # Функция удаления пути к видеофайлу
    def removeVideoPath(self, i):
        del self._projectData['videoPaths'][i]

    
    # Функция получения списка путей до датасетов
    def getDatasetPaths(self):
        return self._projectData['datasetPaths']
    # Функция изменения списка путей до датасетов
    def setDatasetPaths(self, datasetPaths=list()):
        self._projectData['datasetPaths'] = datasetPaths
    # Функция изменения пути до датасета
    def setDatasetPath(self, i, datasetPath):
        self._projectData['datasetPaths'][i] = datasetPath
    # Функция добавления пути к новому датасету
    def addDatasetPath(self, datasetPath):
        if datasetPath not in self._projectData['datasetPaths']:
            self._projectData['datasetPaths'].append(datasetPath)
    # Функция удаления пути к датасету
    def removeDatasetPath(self, i):
        del self._projectData['datasetPaths'][i]


    # Функция получения последнего использованного трекера
    def getTrackerType(self):
        return self._projectData['trackerType']
    # Функция изменения последнего использованного трекера
    def setTrackerType(self, trackerType='csrt'):
        self._projectData['trackerType'] = trackerType


    def getConfidenceThreshold(self):
        return self._projectData['confidenceThreshold']
    def setConfidenceThreshold(self, confidenceThreshold=0.5):
        self._projectData['confidenceThreshold'] = confidenceThreshold


    def getNMSThreshold(self):
        return self._projectData['nmsThreshold']
    def setNMSThreshold(self, nmsThreshold=0.5):
        self._projectData['nmsThreshold'] = nmsThreshold
