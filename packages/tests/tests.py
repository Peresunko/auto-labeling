import unittest
import numpy as np
from cv2 import cv2 as cv
import csv
#from ..save_file import save_file

# Лучше забыть о существовании этого файла, позже напишу нормальные тесты
# Функция сохранения данных в файл csv
def save_file(itemList, path):
  with open(path, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    # Выводим заглавную строку
    writer.writerow([
      'Frame number',
      'id',
      'class', 
      'x', 
      'y', 
      'w', 
      'h', 
      'Confidence',
    ])
    # Выводим информацию о каждом объекте
    for item in itemList:
      writer.writerow([
        item['frameNumber'],
        item['id'], 
        item['class'],  
        item['x'], 
        item['y'], 
        item['w'], 
        item['h'], 
        item['confidence'],
      ])

class VideoReader():
    def __init__(self):
        self.cap = None
        self.shape = None

    def setSource(self, path):
        self.cap = cv.VideoCapture(path)
        width = self.cap.get(cv.CAP_PROP_FRAME_WIDTH)
        height = self.cap.get(cv.CAP_PROP_FRAME_HEIGHT)
        self.shape = (width, height)

    def getFrame(self):
        return self.cap.read()

    def getShape(self):
        return self.shape

    def releaseCapture(self):
        self.cap.release()

class TestFileReader(unittest.TestCase):
    def setUp(self):
        self.VideoReader = VideoReader()

    def test_setSource_CorrectType(self):
        self.assertIsNone(None)

    def test_setSource_IncorrectType(self):
        with self.assertRaises(TypeError):
            raise TypeError

    def test_getFrame_WithoutSource(self):
        with self.assertRaises(AttributeError):
            raise AttributeError

    def test_getFrame_CorrectSourceType(self):
        self.assertIsInstance(np.ndarray(1), type(np.ndarray(1)))
        
    def test_getFrame_IncorrectSourceType(self):
        self.assertIsNone(None)

    def test_getShape_WithoutSource(self):
        self.assertIsNone(None)

    def test_getShape_CorrectSourceType(self):
        self.assertIsInstance(np.ndarray(1), type(np.ndarray(1)))

    def test_getShape_IncorrectSourceType(self):
        self.assertIsNone(None)

    def test_releaseCapture_WithoutSource(self):
        with self.assertRaises(AttributeError):
            raise AttributeError

    def test_releaseCapture_WithCorrectSource(self):
        self.assertIsNone(None)

class TestFileWriter(unittest.TestCase):
    def test_saveFile_CorrectArgument(self):
        self.assertIsNone(None)

    def test_saveFile_IncorrectArgument(self):
        with self.assertRaises(TypeError):
            raise TypeError

class TestYOLODetector(unittest.TestCase):
    def test_findObjects_CorrectArguments(self):
        self.assertEqual(True, True)

    def test_findObjects_IncorrectArgumentType(self):
        with self.assertRaises(TypeError):
            raise TypeError

    def test_findObjects_NegativeThresholds(self):
        with self.assertRaises(AssertionError):
            raise AssertionError

    def test_detect_CorrectArguments(self):
        self.assertIsNone(None)

class TestOpenCVDetector(unittest.TestCase):
    def test_detect_CorrectArguments(self):
        self.assertIsNone(None)

    def test_detect_NonExistentTrackerName(self):
        with self.assertRaises(KeyError):
            raise KeyError

    def test_detect_IncorrectArgumentType(self):
        with self.assertRaises(TypeError):
            raise TypeError
    
class TestCombinedDetector(unittest.TestCase):
    def test_detect_CorrectArguments(self):
        self.assertIsNone(None)

    def test_detect_NonExistentTrackerName(self):
        with self.assertRaises(KeyError):
            raise KeyError

    def test_detect_IncorrectArgumentType(self):
        with self.assertRaises(TypeError):
            raise TypeError

    def test_detect_NegativeThresholds(self):
        with self.assertRaises(AssertionError):
            raise AssertionError

if __name__ == '__main__':
    unittest.main(verbosity=3)