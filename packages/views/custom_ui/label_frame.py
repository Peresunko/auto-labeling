from PyQt5.QtWidgets import QMessageBox, QLabel
from PyQt5.QtCore import QPoint, QRect, Qt, pyqtSignal
from PyQt5.QtGui import QFont, QPainter, QPen


# Класс лейбла-мутанта с возможностью рисования прямоугольников поверх пиксмапа
class LabelFrame(QLabel):
    # objectWasSelected - при выборе объекта отправляем сигналом информацию о нем для заполнения полей ID и класса
    # objectWasDeselected - если выделение было отменено, то сигнал отправляем для отключения полей редактирования
    objectWasSelected = pyqtSignal(dict)
    objectWasDeselected = pyqtSignal()


    def __init__(self, parent=None):
        super().__init__(parent)
        # Константы режима работы лейбла
        # View - просмотр кадра, Draw - рисование прямоугольников, Edit - редактирование прямоугольников
        self.MODE_VIEW = 0
        self.MODE_DRAW = 1
        self.MODE_EDIT = 2

        # Константы для определения сторон выбранного прямоугольника
        # T - Top, L - Left, R - Right, I - Inside, B - Bottom, O - Outside
        self.SIDE_TL = 0
        self.SIDE_T = 1
        self.SIDE_TR = 2
        self.SIDE_L = 3
        self.SIDE_I = 4
        self.SIDE_R = 5
        self.SIDE_BL = 6
        self.SIDE_B = 7
        self.SIDE_BR = 8
        self.SIDE_O = 9
    
        # x и y - при рисовании для координат начала и конца, при редактировании - когда как (запоминание прошлых позиций мышки часто)
        self._x0 = 0
        self._y0 = 0
        self._x1 = 0
        self._y1 = 0
    
        # workMode - для изменения поведения ивентов в зависимости от режима работы
        # changingSide - запоминание изменяемой стороны, чтобы при быстром движении мышки не менялось действие редактирования
        # flagClickAndDrag - для отслеживания того, нажали мы и тянем ли мышку
        # flagChangeRect - при редактировании для разграничения обычного клика и изменения выделенного прямоугольника
        self._workMode = self.MODE_VIEW
        self._changingSide = self.SIDE_O
        self._flagClickAndDrag = False
        self._flagChangeRect = False

        # rect - текущий прямоугольник (рисуемый или выделенный (элемент списка currentFrameRects))
        # selectedRectInfo - информация о текущем прямоугольнике (элемент списка currentFrameObjectsInfo)
        # frameNumber - номер текущего кадра, для сохранения данных о новых объектах
        # currentFrameRects - все прямоугольники текущего кадра
        # selectedRectZones - это при тестировании использовал, для отрисовки зон выделенного прямоугольника
        # currentFrameObjectsInfo - информация обо всех объектах на кадре
        self._rect = None
        self._selectedRectInfo = None
        self._frameNumber = 0
        self._currentFrameRects = []
        #self.selectedRectZones = []
        self._currentFrameObjectsInfo = []


    # Для получения инфы об объектах кадра
    def getCurrentFrameInfo(self):
        return self._currentFrameObjectsInfo


    # Для установки информации об объектах текущего кадра
    # Мне лень переписывать все взаимодействия с currentFrameRects,
    # Поэтому пусть храним исходную инфу об объектах в отдельном поле, в currentFrameRects - так же
    # QRect каждого объекта, а в rect - QRect выбранного объекта
    def setCurrentFrameRects(self, frameObjectsInfo, i):
        self._rect = None
        self._selectedRectInfo = None
        self._currentFrameObjectsInfo = frameObjectsInfo
        self._currentFrameRects.clear()
        for frameObject in frameObjectsInfo:
            self._currentFrameRects.append(QRect(frameObject['x'], frameObject['y'], frameObject['w'], frameObject['h']))
        self._frameNumber = i
        self.update()


    # Для обновления информации об объекте (вызывается в слоте кнопки сохранения инфы об объекте)
    def setSelectedObjectInfo(self, info):
        self._selectedRectInfo['id'] = info['id']
        self._selectedRectInfo['class'] = info['class']
        self._selectedRectInfo['x'] = self._rect.left()
        self._selectedRectInfo['y'] = self._rect.top()
        self._selectedRectInfo['w'] = self._rect.width()
        self._selectedRectInfo['h'] = self._rect.height()
        self.update()


    def deleteSelectedObject(self):
        del self._currentFrameObjectsInfo[self._currentFrameRects.index(self._rect)]
        del self._currentFrameRects[self._currentFrameRects.index(self._rect)]
        self._rect = None
        self._selectedRectInfo = None
        self.objectWasDeselected.emit()
        self._flagChangeRect = False
        self.update()


    # Для изменения режима извне через комбобокс (по индексу выбранного поля комбобокса)
    # Наверна общее можно вынести, но пусть так и будет)00
    def setWorkMode(self, mode):
        if mode == self.MODE_DRAW:
            self.objectWasDeselected.emit()
            self._workMode = self.MODE_DRAW
            self.setMouseTracking(False)
            self.setCursor(Qt.CrossCursor)
            self._rect = None
            self._selectedRectInfo = None
            self._x0 = 0
            self._x1 = 0
            self._y0 = 0
            self._y1 = 0
            self.update()
        elif mode == self.MODE_EDIT:
            self.objectWasDeselected.emit()
            self._workMode = self.MODE_EDIT
            self.setMouseTracking(True)
            self.setCursor(Qt.ArrowCursor)
            self._rect = None
            self._selectedRectInfo = None
            self.update()
        else:
            self.objectWasDeselected.emit()
            self._workMode = self.MODE_VIEW
            self.setMouseTracking(False)
            self.setCursor(Qt.ArrowCursor)
            self._rect = None
            self._selectedRectInfo = None
            self.update()


    # При нажатии мышки:
    # Draw: фиксируем стартовые координаты и запоминаем, что начали рисовать
    # Edit: запоминаем стартовые координаты для перемещения
    # View: ничего
    def mousePressEvent(self,event):
        # Если режим рисования
        if self._workMode is self.MODE_DRAW:
            self._flagClickAndDrag = True
            self._x0 = event.x()
            self._y0 = event.y()
        # Если режим редактирования И в момент нажатия мышка находилась в районе выбранного прямоугольника
        # Для выбора текущего выделенного прямоугольника нажатие не важно, важно событие отпускания мыши
        if self._workMode is self.MODE_EDIT and self._flagChangeRect:
            self._flagClickAndDrag = True
            self._x0 = event.x()
            self._y0 = event.y()


    # При отпускании мышки:
    # Draw: если отпустили мышку - больше не рисуем, сохраняем информацию об объекте
    # Edit: находим прямоугольник, который был выбран
    # View: ничего
    def mouseReleaseEvent(self,event):
        # При рисовании сбрасываем флаг перетаскивания мышки, добавляем текущий рект в список, сам рект обнуляем
        if self._workMode is self.MODE_DRAW:
            self._flagClickAndDrag = False
            self._currentFrameRects.append(self._rect)
            self._currentFrameObjectsInfo.append({
                'frameNumber': self._frameNumber,
                'id': len(self._currentFrameObjectsInfo),
                'class': 'unknown',
                'x': self._rect.left(),
                'y': self._rect.top(),
                'w': self._rect.width(),
                'h': self._rect.height(),
                'confidence': '-',
            })
            self._rect = None


        # В режиме редактирования:
        if self._workMode is self.MODE_EDIT:
            # Если мы перетаскивали мышь - перестаем
            if self._flagClickAndDrag:
                self._flagClickAndDrag = False
            # Иначе меняем по возможности выбранный прямоугольник
            else:
                self._x1 = event.x()
                self._y1 = event.y()
                contain = []

                # Ну все, костыли поехали
                # UPD: походу отсюда баг появился - если выделить объект, а потом, не двигая мышку, дважды нажать и перетащить в границах объекта
                # (тип пытаемся перетащить прямоугольник, но мышка в виде стрелки, а не руки, ибо не двигали мышку), то объект перестанет быть выделенным,
                # мышка на всем кадре в виде руки. Но вроде выбор другого объекта фиксит это
                i = -1
                if self._rect is not None:
                    i = self._currentFrameRects.index(self._rect)

                self._currentFrameRects.clear()
                for frameObject in self._currentFrameObjectsInfo:
                    self._currentFrameRects.append(QRect(frameObject['x'], frameObject['y'], frameObject['w'], frameObject['h']))

                j = 0
                for rect in self._currentFrameRects:
                    if rect.contains(self._x1, self._y1):
                        if i != j:
                            contain.append(rect)
                    j += 1
                
                if len(contain) > 1:
                    QMessageBox.warning(self, "Внимание", "Для выбора прямоугольника нажмите туда, где нужный прямоугольник не пересекается с другими!")
                elif len(contain) == 1:
                    self._rect = contain[0]
                    self._selectedRectInfo = self._currentFrameObjectsInfo[self._currentFrameRects.index(self._rect)]
                    self.objectWasSelected.emit(self._selectedRectInfo)
                else:
                    self._rect = None
                    self._selectedRectInfo = None
                    self.objectWasDeselected.emit()
                    self._flagChangeRect = False
        self.update()

    # Пока рисуем - обновляем конечные координаты
    def mouseMoveEvent(self,event):
        # Если рисуем, то обновляем текущие/конечные координаты и обновляем рисунок
        if self._flagClickAndDrag and self._workMode is self.MODE_DRAW:
            self._x1 = event.x()
            self._y1 = event.y()        
            self.update()
        # Если редактируем и есть цель редактирования:
        if self._rect is not None and self._workMode is self.MODE_EDIT:
            # составляем прямоугольники для каждой зоны - всех углов, всех сторон и внутренностей
            # wh - магическая переменная ширинавысота, которая может быть возможно и не нужна, но лучше пусть будет
            # она для расчета минимальных размеров прямоугольника при редактировании используется, чтобы нельзя было
            # слишком маленький сделать
            x = event.x()
            y = event.y()
            wh = 2
            right = self._rect.right()
            left = self._rect.left()
            top = self._rect.top()
            bottom = self._rect.bottom()
            leftBorder = QRect(left - wh, top + wh, wh * 2 + 1, bottom - top - wh * 2)
            rightBorder = QRect(right - wh, top + wh, wh * 2 + 1, bottom - top - wh * 2)
            topBorder = QRect(left + wh, top - wh, right - left - wh * 2, wh * 2 + 1)
            bottomBorder = QRect(left + wh, bottom - wh, right - left - wh * 2, wh * 2 + 1)
            tlCorner = QRect(left - wh, top - wh, wh * 2 + 1, wh * 2 + 1)
            trCorner = QRect(right - wh, top - wh, wh * 2 + 1, wh * 2 + 1)
            blCorner = QRect(left - wh, bottom - wh, wh * 2 + 1, wh * 2 + 1)
            brCorner = QRect(right - wh, bottom - wh, wh * 2 + 1, wh * 2 + 1)
            inside = QRect(left + wh * 3, top + wh * 3, right - left - wh * 6, bottom - top - wh * 6)
            #self.selectedRectZones = [leftBorder, rightBorder, topBorder, bottomBorder, tlCorner, trCorner, blCorner, brCorner, inside]
            print("X:", x, "Y:", y, "Current Side:", self._changingSide)

            # если в это время тащили мышь в районе прямоугольника, то в зависимости от перетаскиваемой стороны редактируем прямоугольник
            # изменение право и нижней сторон почему-то двигает прямоугольник, поэтому шакалю через изменение угла
            # UPD: право и низ определяются как центр? Странна нипанятна
            # UPD2: это я шакал, g в changingSide в двух местах не поставил где-то))00
            # UPD3: есть некоторые проблемы при взаимодействии прямугольника с границами кадра, но не слишком критично
            if self._flagClickAndDrag and self.pixmap().rect().contains(x, y, True):
                if self._changingSide is self.SIDE_I:
                    if self.pixmap().rect().contains(self._rect.translated(x - self._x0, y - self._y0), True): 
                        self._rect.translate(x - self._x0, y - self._y0)
                    print("Внутри прямоугольника")
                if self._changingSide is self.SIDE_L and (self._rect.width() >= wh * 6 or x < left):
                    self._rect.setLeft(x)
                    print("Левая граница")
                if self._changingSide is self.SIDE_R and (self._rect.width() >= wh * 6 or x > right):
                    self._rect.setRight(x)
                    print("Правая граница")
                if self._changingSide is self.SIDE_T and (self._rect.height() >= wh * 6 or y < top):
                    self._rect.setTop(y)
                    print("Верхняя граница")
                if self._changingSide is self.SIDE_B and (self._rect.height() >= wh * 6 or y > bottom):
                    self._rect.setBottom(y)
                    print("Нижняя граница")
                if self._changingSide is self.SIDE_TL and self._rect.height() >= wh * 6 and self._rect.width() >= wh * 6:
                    self._rect.setTopLeft(QPoint(x, y))
                    print("Левый верхний угол")
                if self._changingSide is self.SIDE_TR and self._rect.height() >= wh * 6 and self._rect.width() >= wh * 6:
                    self._rect.setTopRight(QPoint(x, y))
                    print("Правый верхний угол")
                if self._changingSide is self.SIDE_BL and self._rect.height() >= wh * 6 and self._rect.width() >= wh * 6:
                    self._rect.setBottomLeft(QPoint(x, y))
                    print("Левый нижний угол")
                if self._changingSide is self.SIDE_BR and self._rect.height() >= wh * 6 and self._rect.width() >= wh * 6:
                    self._rect.setBottomRight(QPoint(x, y))
                    print("Правый нижний угол")
                
                self._x0 = x
                self._y0 = y

            # Иначе обрабатываем внешний вид курсора в зависимости от позиции относительно выбранного прямоугольника
            # Тут же определяем, приведет ли щелчок мыши к началу редактирования, а также какую сторону редачим
            else:
                if leftBorder.contains(x, y) or rightBorder.contains(x, y):
                    self.setCursor(Qt.SizeHorCursor)
                    self._flagChangeRect = True
                    if leftBorder.contains(x, y):
                        self._changingSide = self.SIDE_L
                        print("Левая граница")
                    elif rightBorder.contains(x, y):
                        self._changingSide = self.SIDE_R
                        print("Правая граница")
                elif topBorder.contains(x, y) or bottomBorder.contains(x, y):
                    self.setCursor(Qt.SizeVerCursor)
                    self._flagChangeRect = True
                    if topBorder.contains(x, y):
                        self._changingSide = self.SIDE_T
                        print("Верхняя граница")
                    elif bottomBorder.contains(x, y):
                        self._changingSide = self.SIDE_B
                        print("Нижняя граница")
                elif tlCorner.contains(x, y) or brCorner.contains(x, y):
                    self.setCursor(Qt.SizeFDiagCursor)
                    self._flagChangeRect = True
                    if tlCorner.contains(x, y):
                        self._changingSide = self.SIDE_TL
                        print("Левый верхний угол")
                    elif brCorner.contains(x, y):
                        self._changingSide = self.SIDE_BR
                        print("Правый нижний угол")
                elif trCorner.contains(x, y) or blCorner.contains(x, y):
                    self.setCursor(Qt.SizeBDiagCursor)
                    self._flagChangeRect = True
                    if trCorner.contains(x, y):
                        self._changingSide = self.SIDE_TR
                        print("Правый верхний угол")
                    elif blCorner.contains(x, y):
                        self._changingSide = self.SIDE_BL
                        print("Левый нижний угол")
                elif inside.contains(x, y):
                    self.setCursor(Qt.OpenHandCursor)
                    self._flagChangeRect = True
                    self._changingSide = self.SIDE_I
                    print("Внутри прямоугольника")
                else:
                    self.setCursor(Qt.ArrowCursor)
                    self._flagChangeRect = False
                    self._changingSide = self.SIDE_O
                    print("Вне прямоугольника")

            self.update()


    # Рисование
    def paintEvent(self, event):
        super().paintEvent(event)

        defaultPen = QPen(Qt.red, 3, Qt.SolidLine)
        chosenPen = QPen(Qt.green, 3, Qt.DashLine)
        painter = QPainter(self)

        # Рисование прямоугольника в зависимости от позиции мышки по сравнению с начальной позицией
        if self._workMode is self.MODE_DRAW:
            if self._x1 < self._x0:
                if self._y1 < self._y0:
                    self._rect = QRect(self._x1, self._y1, abs(self._x1-self._x0), abs(self._y1-self._y0))
                else:
                    self._rect = QRect(self._x1, self._y0, abs(self._x1-self._x0), abs(self._y1-self._y0))
            else:
                if self._y1 < self._y0:
                    self._rect = QRect(self._x0, self._y1, abs(self._x1-self._x0), abs(self._y1-self._y0))
                else:
                    self._rect = QRect(self._x0, self._y0, abs(self._x1-self._x0), abs(self._y1-self._y0))

        # Отрисовка всех прямоугольников текущего кадра
        i = 0

        for rect in self._currentFrameRects:
            if rect is not self._rect:
                painter.setPen(defaultPen)
                painter.setFont(QFont('Decorative', 10))
                painter.drawRect(rect)
                painter.drawText(QPoint(rect.left(), rect.top() -5), 'Class: {}'.format(self._currentFrameObjectsInfo[i]['class']))
                painter.drawText(QPoint(rect.left(), rect.top() -15), 'ID: {}'.format(self._currentFrameObjectsInfo[i]['id']))
            i += 1
        
        if self._rect is not None:
            if self._workMode is self.MODE_DRAW:
                painter.setPen(defaultPen)
                painter.drawRect(self._rect)
            elif self._workMode is self.MODE_EDIT:
                i = self._currentFrameRects.index(self._rect)
                painter.setPen(chosenPen)
                painter.setFont(QFont('Decorative', 10))
                painter.drawRect(self._rect)
                painter.drawText(QPoint(self._rect.left(), self._rect.top() -5), 'Class: {}'.format(self._currentFrameObjectsInfo[i]['class']))
                painter.drawText(QPoint(self._rect.left(), self._rect.top() -15), 'ID: {}'.format(self._currentFrameObjectsInfo[i]['id']))
                #painter.setPen(defaultPen)
                #for rect in self.selectedRectZones:
                #    painter.drawRect(rect)
