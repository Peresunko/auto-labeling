from PyQt5.QtWidgets import QApplication, QMessageBox, QFileDialog, QDialog
from PyQt5.uic import loadUi
from PyQt5 import QtCore, QtGui

import sys
import os

if __name__ == "__main__":
    from resources import startup_window_resources


# Класс диалога создания нового проекта, на котором задаем название и расположение проекта
class NewProjectDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = loadUi("designer/new_project_dialog.ui", self)
        self._connectSignals()
        self.setDefaultValues()


                                                                # Публичные функции окна


    # Функция установки дефолтных значений
    # параметры даются контроллером из менеджера глобальных настроек
    def setDefaultValues(self, defaultProjectName="OwO", defaultProjectsFolder="wats this", createSubfolder=True):
        self.setNewProjectName(defaultProjectName)
        self.setNewProjectFolder(defaultProjectsFolder)
        self.setCreateSubfolderCheckBoxValue(createSubfolder)
        self._slotUpdateProjectFilesFolderLabel()


    # Чтобы не обращаться постоянно через self.ui к элементам, пусть будет так
    # Методы для доступа к полю имени проекта
    def getNewProjectName(self):
        return self.ui.lineEditProjectName.text()
    def setNewProjectName(self, projectName):
        self.ui.lineEditProjectName.setText(projectName)

    # Методы для доступа к полю папки проекта
    def getNewProjectFolder(self):
        return self.ui.lineEditProjectFolder.text()
    def setNewProjectFolder(self, projectFolder):
        self.ui.lineEditProjectFolder.setText(projectFolder)

    # Методы для доступа к значению чек-бокса для создания папки с именем проекта в указанной папке
    def getCreateSubfolderCheckBoxValue(self):
        return self.ui.checkBoxCreateSubfolder.isChecked()
    def setCreateSubfolderCheckBoxValue(self, createSubfolder):
        self.ui.checkBoxCreateSubfolder.setChecked(createSubfolder)

    # Методы для доступа к значению лейбла с окончательным путем до файлов проекта
    def getProjectFilesFolderLabel(self):
        return self.ui.labelProjectFilesFolder.text()
    def setProjectFilesFolderLabel(self, projectFilesFolder):
        self.ui.labelProjectFilesFolder.setText(projectFilesFolder)


                                                                # Приватные функции окна

    
    # Не соединяю слоты у двух кнопок, потому что ОК/Отмена в контроллере описывается (т.к. там взаимодействие между окнами)
    def _connectSignals(self):
        self.ui.pushBtnBrowse.clicked.connect(lambda: self._slotSelectProjectFolder())
        self.ui.checkBoxCreateSubfolder.stateChanged.connect(lambda: self._slotUpdateProjectFilesFolderLabel())
        self.ui.lineEditProjectName.textChanged.connect(lambda: self._slotUpdateProjectFilesFolderLabel())
        self.ui.lineEditProjectFolder.textChanged.connect(lambda: self._slotUpdateProjectFilesFolderLabel())


                                                                # Слоты для функционала в пределах окна


    # Слот для выбора папки для проекта
    def _slotSelectProjectFolder(self):
        path = QFileDialog.getExistingDirectory(self, 'Выберите папку для нового проекта', directory=self.getNewProjectFolder())
        if path:
            path = os.path.normpath(path)
            self.setNewProjectFolder(path)


    # Слот для события изменения текста в полях ввода
    def _slotUpdateProjectFilesFolderLabel(self):
        projectFolder = self.getNewProjectFolder()
        projectName = self.getNewProjectName()

        if self.getCreateSubfolderCheckBoxValue():
            self.setProjectFilesFolderLabel(os.path.join(projectFolder, projectName))
        else:
            self.setProjectFilesFolderLabel(projectFolder)



if __name__ == "__main__":
    app = QApplication(sys.argv)

    # Иконки
    appIcon = QtGui.QIcon()
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(16,16))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(24,24))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(32,32))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(48,48))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(256,256))

    app.setWindowIcon(appIcon)
    
    dial = NewProjectDialog()
    dial.show()
    sys.exit(app.exec())