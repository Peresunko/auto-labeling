from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QPushButton
from PyQt5.uic import loadUi
from PyQt5.Qt import QCompleter, QDesktopServices, QIcon, QImage, QIntValidator, QMenu, QMessageBox, QPixmap, QUrl
from PyQt5 import QtCore, QtGui
from cv2 import cv2 as cv

import sys
import random

if __name__ == "__main__":
    from resources import workspace_window_resources
    from custom_ui.label_frame import LabelFrame
else:
    from packages.views.resources import workspace_window_resources
    from packages.views.custom_ui.label_frame import LabelFrame


# Класс окна работы с открытым проектом
class WorkspaceWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = loadUi("designer/workspace_window.ui", self)
        self.ui.labelVideoFrame = LabelFrame(self)
        self.ui.gridLayoutFrame.addWidget(self.ui.labelVideoFrame, 0, 0)
        
        self.SENDER_BTN_VIEW_MODE = self.ui.pushBtnViewMode
        self.SENDER_BTN_DRAW_MODE = self.ui.pushBtnDrawMode
        self.SENDER_BTN_EDIT_MODE = self.ui.pushBtnEditMode

        self.UI_STATE_VIDEO_NOT_OPENED = 0
        self.UI_STATE_VIDEO_OPENED = 1
        self.UI_STATE_MULTITHREADING = 2
        
        self.ui.groupBoxSelectedObjectInfo.setEnabled(False)

        self._fps = 30
        self._classNameList = []
        self._filterClassNames = []
        self._classCompleter = QCompleter(self._classNameList)
        self._classCompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        
        # Это нужно применять только при использовании стилей, сейчас они отключены, поэтому это в комментах
        #self.ui.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        #self.ui.setWindowFlags(QtCore.Qt.NoDropShadowWindowHint)
        #self.ui.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)

        # Для только числовых значений
        self._onlyInt = QIntValidator()
        self.ui.lineEditFrameWidth.setValidator(self._onlyInt)
        self.ui.lineEditFrameHeight.setValidator(self._onlyInt)
        self.ui.lineEditObjectID.setValidator(self._onlyInt)
        self.ui.lineEditGOTOFrame.setValidator(self._onlyInt)

        self._connectSignals()
        self.setDefaultValues()
        self.adjustSize()
    

                                                                # Публичные функции окна


    # Функция установки значений по умолчанию
    def setDefaultValues(self, projectName="Dummy", confidence=0.5, nms=0.5, tracker='csrt'):
        # По параметрам
        self.setWindowTitle("Открытый проект - {0}".format(projectName))
        self.setConfidenceThreshold(confidence)
        self.setNMSThreshold(nms)
        self.setSelectedTrackerType(tracker)
        self.switchStateOfUIElements(self.UI_STATE_VIDEO_NOT_OPENED)

        # Общие

    # Функция переключения состояния окна работы с проектом
    # Разные элементы в разные моменты времени надо отрубать, тут состояния описываются
    # TODO: для menubar ещё добавить надо отключения некоторые
    def switchStateOfUIElements(self, state):
        # Состояние, когда никакое видео не открыто (только открыли проект)
        if state is self.UI_STATE_VIDEO_NOT_OPENED:
            # Отключаем элемент кадра, его инструменты и панель перехода по кадрам
            self.ui.labelVideoFrame.setEnabled(False)
            self.changeWorkMode(self.SENDER_BTN_VIEW_MODE)
            self.SENDER_BTN_VIEW_MODE.setEnabled(False)
            self.SENDER_BTN_DRAW_MODE.setEnabled(False)
            self.SENDER_BTN_EDIT_MODE.setEnabled(False)
            self.ui.frameVideoControls.setEnabled(False)

            # На вкладке "Разметка" отключаем ручную разметку и панель управления
            self.ui.groupBoxControlPanel.setEnabled(False)
            self.ui.groupBoxSelectedObjectInfo.setEnabled(False)

            # На вкладке "Видео" отключаем загрузку датасета и именение размера кадра
            self.ui.groupBoxLoadDataset.setEnabled(False)
            self.ui.groupBoxFrameSize.setEnabled(False)

        # Состояние, когда видео открыли и оно просто есть
        # Это и при открытии видео, и при остановке обработки
        elif state is self.UI_STATE_VIDEO_OPENED:
            # Включаем элемент кадра, его инструменты и панель перехода по кадрам
            self.ui.labelVideoFrame.setEnabled(True)
            self.SENDER_BTN_VIEW_MODE.setEnabled(True)
            self.SENDER_BTN_DRAW_MODE.setEnabled(True)
            self.SENDER_BTN_EDIT_MODE.setEnabled(True)
            self.changeWorkMode(self.SENDER_BTN_VIEW_MODE)
            self.ui.frameVideoControls.setEnabled(True)

            # На вкладке "Разметка" включаем панель управления и фильтрацию
            self.ui.groupBoxControlPanel.setEnabled(True)
            self.ui.YOLOFilter.setEnabled(True)
            self.ui.groupBoxSelectedObjectInfo.setEnabled(False)

            # На вкладке "Видео" включаем всю вкладку
            self.ui.tabVideo.setEnabled(True)
            self.ui.groupBoxLoadDataset.setEnabled(True)
            self.ui.groupBoxFrameSize.setEnabled(True)

            # На вкладке "Параметры детектора" включаем всю вкладку 
            self.ui.tabDetectorParameters.setEnabled(True)  

        # Состояние, когда кадр обновляется из второго потока
        # В этом состоянии надо опять-таки отобрать у пользователя возможности некоторые
        elif state is self.UI_STATE_MULTITHREADING:
            # Отключаем кадра инструменты и панель перехода по кадрам
            self.ui.labelVideoFrame.setEnabled(True)
            self.changeWorkMode(self.SENDER_BTN_VIEW_MODE)
            self.SENDER_BTN_VIEW_MODE.setEnabled(False)
            self.SENDER_BTN_DRAW_MODE.setEnabled(False)
            self.SENDER_BTN_EDIT_MODE.setEnabled(False)
            self.ui.frameVideoControls.setEnabled(False)

            # На вкладке "Разметка" отключаем ручную разметку и фильтрацию поиска
            self.ui.groupBoxSelectedObjectInfo.setEnabled(False)
            self.ui.YOLOFilter.setEnabled(False)

            # На вкладке "Видео" отключаем всю вкладку "Видео"
            self.ui.tabVideo.setEnabled(False)

            # На вкладке "Параметры детектора" отключаем всю вкладку 
            self.ui.tabDetectorParameters.setEnabled(False)         

    # Функция для сохранения изменений в данные выбранного объекта
    def saveSelectedObjectInfo(self):
        info = {}
        info['id'] = self.ui.lineEditObjectID.text()
        info['class'] = self.ui.lineEditSelectedObjectClass.text()

        if info['id'] and info['class']:
            self.ui.labelVideoFrame.setSelectedObjectInfo(info)

    # Функция для удаления выбранного объекта
    def deleteSelectedObject(self):
        self.ui.labelVideoFrame.deleteSelectedObject()

    # Функция для обновления кадра
    def updateFrameData(self, frame, i, frameInfo):
        self.ui.labelCurrentFrameNumber.setText(str(i))
        self.ui.hSliderVideo.setValue(i)

        ret, img = frame

        if ret:
            height, width, bytesPerComponent = img.shape
            bytesPerLine = 3 * width
            cv.cvtColor(img, cv.COLOR_BGR2RGB, img)
            QImg = QImage(img.data, width, height, bytesPerLine,QImage.Format_RGB888)
            pixmap = QPixmap.fromImage(QImg)
            self.ui.labelVideoFrame.setPixmap(pixmap)

        self.ui.labelVideoFrame.setCurrentFrameRects(frameInfo, i)

    # Функция для переименовывания кнопки начала/остановки отслеживания
    def renameStartStopButtonOpenCV(self, running):
        if running:
            self.ui.pushBtnStartStopTracking.setText('Остановить отслеживание объектов')
        else:
            self.ui.pushBtnStartStopTracking.setText('Начать отслеживание объектов')

    # Функция для переименовывания кнопки начала/остановки распознавания
    def renameStartStopButtonYOLO(self, running):
        if running:
            self.ui.pushBtnStartStopSearch.setText('Остановить поиск объектов')
        else:
            self.ui.pushBtnStartStopSearch.setText('Найти объекты на всех кадрах')

    # Функция для смены иконки кнопки проигрывания
    def switchPlayButtonIcon(self, running):
        if running:
            self.ui.pushBtnPlay.setIcon(QIcon(":/resources/icons/stop_button.png"))
        else:
            self.ui.pushBtnPlay.setIcon(QIcon(":/resources/icons/play_button.png"))
  

    # Методы для доступа к кнопкам выбора трекера
    def getSelectedTrackerType(self):
        if self.ui.radioBtnCSRT.isChecked():
            return 'csrt'
        elif self.ui.radioBtnBoosting.isChecked():
            return 'boosting'
        elif self.ui.radioBtnKCF.isChecked():
            return 'kcf'
        elif self.ui.radioBtnMIL.isChecked():
            return 'mil'
        elif self.ui.radioBtnMOSSE.isChecked():
            return 'mosse'
        elif self.ui.radioBtnMedianFlow.isChecked():
            return 'medianflow'
        elif self.ui.radioBtnTLD.isChecked():
            return 'tld'
    def setSelectedTrackerType(self, tracker):
        if tracker == 'csrt':
            self.ui.radioBtnCSRT.setChecked(True)
        elif tracker == 'boosting':
            self.ui.radioBtnBoosting.setChecked(True)
        elif tracker == 'kcf':
            self.ui.radioBtnKCF.setChecked(True)
        elif tracker == 'mil':
            self.ui.radioBtnMIL.setChecked(True)
        elif tracker == 'mosse':
            self.ui.radioBtnMOSSE.setChecked(True)
        elif tracker == 'medianflow':
            self.ui.radioBtnMedianFlow.setChecked(True)
        elif tracker == 'tld':
            self.ui.radioBtnTLD.setChecked(True)

    # Методы для доступа к спинбоксам с выбором порогов
    def getConfidenceThreshold(self):
        return self.ui.dblSpinBoxConfidenceThreshold.value()
    def setConfidenceThreshold(self, confidence):
        self.ui.dblSpinBoxConfidenceThreshold.setValue(confidence)
    
    def getNMSThreshold(self):
        return self.ui.dblSpinBoxNMSThreshold.value()
    def setNMSThreshold(self, nms):
        self.ui.dblSpinBoxNMSThreshold.setValue(nms)

    # Методы для доступа к полю расположения видео
    def getVideoPath(self):
        return self.ui.lineEditVideoLocation.text()
    def setVideoPath(self, path):
        self.ui.lineEditVideoLocation.setText(path)

    # Методы для доступа к полю расположения датасета
    def getDatasetPath(self):
        return self.ui.lineEditDatasetLocation.text()
    def setDatasetPath(self, path):
        self.ui.lineEditDatasetLocation.setText(path)

    # Методы для доступа к полям высоты/ширины кадра
    def getPreferredFrameSize(self):
        width = self.ui.lineEditFrameWidth.text()
        height = self.ui.lineEditFrameHeight.text()

        if width and height:
            return True, (int(width), int(height))
        else:
            if not width and not height:
                message = 'Поля ширины и высоты пусты!'
            elif not width:
                message = 'Поле ширины пусто!'
            else:
                message = 'Поле высоты пусто!'

            return False, message
    def setPreferredFrameSize(self, width, height):
        width = str(int(width))
        height = str(int(height))

        self.ui.lineEditFrameWidth.setText(width)
        self.ui.lineEditFrameHeight.setText(height)

    # Методы доступа к полям выбранного объекта
    def getSelectedObjectID(self):
        return self.ui.lineEditObjectID.text()
    def setSelectedObjectID(self, id):
        self.ui.lineEditObjectID.setText(str(id))

    def getSelectedObjectClass(self):
        return self.ui.lineEditSelectedObjectClass.text()
    def setSelectedObjectClass(self, className):
        self.ui.lineEditSelectedObjectClass.setText(className)

    # Методы для доступа к значениям слайдера прогресса видео
    def getSliderValue(self):
        return self.ui.hSliderVideo.value()
    def setSliderValue(self, i):
        maxValue = self.ui.hSliderVideo.maximum()
        minValue = self.ui.hSliderVideo.minimum()
        i = sorted([i, minValue, maxValue])[1]
        self.ui.hSliderVideo.setValue(i)
    def setMaximumSliderValue(self, frameCount):
        self.ui.hSliderVideo.setMaximum(frameCount)  

    # Функция для получения списка классов, которые ЙОЛО должна найти (пустой - все классы)
    def getClassFilterList(self):
        return self._filterClassNames

    # Функция для установки классов, которые ЙОЛО МОЖЕТ найти
    def setClassNameList(self, classNameList):
        self._classNameList = classNameList
        self._classCompleter = QCompleter(self._classNameList)
        self._classCompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.ui.comboBoxClassFilter.setCompleter(self._classCompleter)

    # Функция для изменения частоты кадров
    def setFPS(self, fps):
        self._fps = fps


    # Слот для обработки сигнала смены нажатия на кнопку смены режима кадра
    # В некоторых ситуациях (начало отслеживание) режим стоит переключать на режим просмотра,
    # поэтому эта функцию является ещё и слотом
    def changeWorkMode(self, target=None):
        if target is None:
            target = self.sender()

        if target is self.SENDER_BTN_VIEW_MODE:
            self.ui.labelVideoFrame.setWorkMode(self.ui.labelVideoFrame.MODE_VIEW)
            self.ui.pushBtnViewMode.setEnabled(False)
            self.ui.pushBtnDrawMode.setEnabled(True)
            self.ui.pushBtnEditMode.setEnabled(True)
        elif target is self.SENDER_BTN_DRAW_MODE:
            self.ui.labelVideoFrame.setWorkMode(self.ui.labelVideoFrame.MODE_DRAW)
            self.ui.pushBtnViewMode.setEnabled(True)
            self.ui.pushBtnDrawMode.setEnabled(False)
            self.ui.pushBtnEditMode.setEnabled(True)
        elif target is self.SENDER_BTN_EDIT_MODE:
            self.ui.labelVideoFrame.setWorkMode(self.ui.labelVideoFrame.MODE_EDIT)
            self.ui.pushBtnViewMode.setEnabled(True)
            self.ui.pushBtnDrawMode.setEnabled(True)
            self.ui.pushBtnEditMode.setEnabled(False)


                                                                # Приватные функции окна

    # Функция соединения слотов и сигналов
    def _connectSignals(self):
        # Видео
        self.ui.pushBtnBrowseVideoLocation.clicked.connect(lambda: self._selectVideoFile())
        self.ui.pushBtnBrowseDatasetLocation.clicked.connect(lambda: self._selectDatasetFile())

        # Панель инструментов кадра
        self.ui.pushBtnViewMode.clicked.connect(lambda: self.changeWorkMode())
        self.ui.pushBtnDrawMode.clicked.connect(lambda: self.changeWorkMode())
        self.ui.pushBtnEditMode.clicked.connect(lambda: self.changeWorkMode())

        # Модифицированный лейбл
        self.ui.labelVideoFrame.objectWasSelected.connect(self._displaySelectedObjectInfo)
        self.ui.labelVideoFrame.objectWasDeselected.connect(self._eraseSelectedObjectInfo)

        # Управление кадрами
        self.ui.pushBtnOneSecondBack.clicked.connect(lambda: self._gotoOneSecondBack())
        self.ui.pushBtnOneFrameBack.clicked.connect(lambda: self._gotoOneFrameBack())
        self.ui.pushBtnOneFrameForward.clicked.connect(lambda: self._gotoOneFrameForward())
        self.ui.pushBtnOneSecondForward.clicked.connect(lambda: self._gotoOneSecondForward())
        self.ui.pushBtnGOTOSpecificFrame.clicked.connect(lambda: self._gotoSpecificFrame())
        self.ui.lineEditGOTOFrame.textEdited.connect(lambda: self._gotoSpecificFrameLEValueChanged())

        # Фильтрация ЙОЛО
        self.ui.pushBtnAddClassToFilter.clicked.connect(self._addClassToFilter)
        self.ui.pushBtnRemoveClassFromFilter.clicked.connect(self._removeClassFromFilter)
        
        # Редактирование выбранного объекта
        #self.ui.pushBtnSaveSelectedObjectInfo.clicked.connect(self._saveSelectedObjectInfo)
        #self.ui.pushBtnDeleteSelectedObjectInfo.clicked.connect(self._deleteSelectedObject)



                                                                # Слоты для функционала в пределах окна

    # Слоты для кнопок перехода по кадрам
    def _gotoOneFrameForward(self):
        self.setSliderValue(self.getSliderValue() + 1)
    def _gotoOneFrameBack(self):
        self.setSliderValue(self.getSliderValue() - 1)
    def _gotoOneSecondForward(self):
        self.setSliderValue(self.getSliderValue() + self._fps)
    def _gotoOneSecondBack(self):
        self.setSliderValue(self.getSliderValue() - self._fps)
    def _gotoSpecificFrame(self):
        targetFrame = int(self.ui.lineEditGOTOFrame.text())
        self.setSliderValue(targetFrame)
        self.ui.lineEditGOTOFrame.clear()

    # Слот для изменения значения поля выбора конкретного кадра
    def _gotoSpecificFrameLEValueChanged(self):
        text = self.ui.lineEditGOTOFrame.text()
        if text.lstrip("-+").isdigit():
            targetFrame = int(text)
            minValue = self.ui.hSliderVideo.minimum()
            maxValue = self.ui.hSliderVideo.maximum()
            targetFrame = sorted([targetFrame, minValue, maxValue])[1]
            self.ui.lineEditGOTOFrame.setText(str(targetFrame))

    # Слот для события кадра objectWasSelected
    def _displaySelectedObjectInfo(self, dict):
        self.ui.groupBoxSelectedObjectInfo.setEnabled(True)
        self.ui.lineEditObjectID.setText(str(dict['id']))
        self.ui.lineEditSelectedObjectClass.setText(str(dict['class']))

    # Слот для события кадра objectWasDeselected
    def _eraseSelectedObjectInfo(self):
        self.ui.groupBoxSelectedObjectInfo.setEnabled(False)
        self.ui.lineEditObjectID.clear()
        self.ui.lineEditSelectedObjectClass.clear()

    # Слот для кнопки открытия репозитория
    def _openGitlabLink(self):
        url = QUrl("https://gitlab.com/Peresunko/auto-labeling")
        QDesktopServices.openUrl(url)

    # Слот для выбора видео для обработки
    def _selectVideoFile(self):
        path = QFileDialog.getOpenFileName(self, 'Выберите видео для обработки', '', 'Видео (*.mp4 *.mov *.avi)')
        if path[0]:
            self.setVideoPath(path[0])

    # Слот для выбора датасета для обработки
    def _selectDatasetFile(self):
        path = QFileDialog.getOpenFileName(self, 'Выберите датасет для загрузки', '', 'Датасет (*.csv)')
        if path[0]:
            self.setDatasetPath(path[0])

    # Слот для добавления класса в список классов для поиска
    def _addClassToFilter(self):
        classToAdd = self.ui.comboBoxClassFilter.currentText()
        
        if classToAdd in self._classNameList:
            if classToAdd in self._filterClassNames:
                QMessageBox.warning(self, "Внимание", "Введенный класс уже есть в списке!")
            else:
                self._filterClassNames.append(classToAdd)
                self.ui.listWidgetClassFilter.addItem(classToAdd)
                self.ui.comboBoxClassFilter.clearEditText()
        else:
            QMessageBox.warning(self, "Внимание", "Введенного класса нет в списке классов!\nОбратите внимание на дополнение ввода.")

    # Слот для удаления выбранного в списке класса из списка классов для поиска
    def _removeClassFromFilter(self):
        selectedItems = self.ui.listWidgetClassFilter.selectedItems()
        selectedItemIndex = self.ui.listWidgetClassFilter.currentRow()

        if len(selectedItems) == 0:
            QMessageBox.warning(self, "Внимание", "Выберите из списка класс для удаления!")
        else:
            self._filterClassNames.remove(selectedItems[0].text())
            self.ui.listWidgetClassFilter.takeItem(selectedItemIndex)



 
# Если запустить этот файл, то только это окно откроется
if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    # Иконки
    appIcon = QtGui.QIcon()
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(16,16))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(24,24))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(32,32))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(48,48))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(256,256))

    app.setWindowIcon(appIcon)
    
    win = WorkspaceWindow()
    win.show()
    sys.exit(app.exec())