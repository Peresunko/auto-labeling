from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QPushButton
from PyQt5.uic import loadUi
from PyQt5.Qt import QBitmap, QColor, QDesktopServices, QPainter, QPoint, QRect, QUrl
from PyQt5 import QtCore, QtGui

import os
import sys
import random

if __name__ == "__main__":
    from resources import startup_window_resources
else:
    from packages.views.resources import startup_window_resources


# Класс стартового окна
class StartUpWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = loadUi("designer/startup_window.ui", self)

        # Окно закругленное, тайтл-бар я убрал, поэтому перетягивать его нельзя, это вот для перетягивания надо
        self._mousePressed = False
        self._dragPos = None

        # Эти флаги делают что-то хорошее, что позволяет закруглять края окна
        self.ui.setWindowFlags(QtCore.Qt.WindowFlags() | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowSystemMenuHint)
        self.ui.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)

        self._connectSignals()
        self._roundCorners()
        self.setRecentProjects()
        self.ui.adjustSize()
    

                                                                # Публичные функции окна
                                                                

    # Функция для отображения кнопок для открытия недавно открытых проектов
    # TODO: под разное количество недавних проектов бы надо ещё (более 5, например)
    def setRecentProjects(self, recentProjects=[]):
        recentProjectButtons = self.ui.groupBoxRecentFiles.findChildren(QPushButton)

        [button.hide() for button in recentProjectButtons]

        i = 0

        for project in recentProjects:
            projectName = os.path.basename(project)
            projectName = projectName.split('.')[0]
            print(projectName)
            print(project)
            if i >= 5:
                break

            recentProjectButtons[i].setText(projectName)
            recentProjectButtons[i].setToolTip(project)
            recentProjectButtons[i].show()
            i += 1


                                                                # Приватные функции окна


    # Функция для закругления углов окна
    # DONE: пофиксить закругление нижних углов, почему-то они не круглятся, интернет ответа не дал
    # PS: Проблема была в том, что я в дизайнере на окно не повесил в стилях границу и радиус
    def _roundCorners(self):
        radius = 10.0
        path = QtGui.QPainterPath()
        path.addRoundedRect(QtCore.QRectF(self.ui.rect()), radius, radius)
        mask = QtGui.QRegion(path.toFillPolygon().toPolygon())
        self.ui.setMask(mask)


    # Функция соединения слотов и сигналов
    def _connectSignals(self):
        self.ui.minimizeButton.clicked.connect(self._slotCustomMinimizeButton)
        self.ui.closeButton.clicked.connect(self._slotCustomCloseButton)
        self.ui.pushBtnGitlabLink.clicked.connect(self._slotOpenGitlabLink)
        pass


                                                                # Слоты для функционала в пределах окна


    # Слот для кнопки минимизации
    def _slotCustomMinimizeButton(self):
        self.ui.showMinimized()


    # Слот для кнопки закрытия
    def _slotCustomCloseButton(self):
        self.ui.close()


    # Слот для кнопки открытия репозитория
    def _slotOpenGitlabLink(self):
        url = QUrl("https://gitlab.com/Peresunko/auto-labeling")
        QDesktopServices.openUrl(url)


    
                                                                # Переопределения событий


    # Переопределение события нажатия на кнопку мыши, для перетягивания окна без заголовка
    def mousePressEvent(self, event):
        self._dragPos = event.globalPos()
        self._mousePressed = True


    # Переопределение события перемешения мыши, для перетягивания окна без заголовка
    def mouseMoveEvent(self, event):
        if self._mousePressed:
            self.ui.move(self.ui.pos() + event.globalPos() - self._dragPos)
            self._dragPos = event.globalPos()
            self.ui.update()


    # Переопределение события отпускания кнопки мыши, для перетягивания окна без заголовка
    def mouseReleaseEvent(self, event):
        self._mousePressed = False


 
# Если запустить этот файл, то только это окно откроется
if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    # Иконки
    appIcon = QtGui.QIcon()
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(16,16))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(24,24))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(32,32))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(48,48))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(256,256))

    app.setWindowIcon(appIcon)
    
    win = StartUpWindow()
    win.show()
    sys.exit(app.exec())