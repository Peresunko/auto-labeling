from PyQt5.QtWidgets import QApplication, QMessageBox, QFileDialog, QDialog
from PyQt5.uic import loadUi
from PyQt5 import QtCore, QtGui

import sys
import os

if __name__ == "__main__":
    from resources import workspace_window_resources


# Класс диалога настроек, на котором можно менять настройки приложения (настройки ещё придумать правда надо, но да ладно, не так важно пока)
class SettingsDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = loadUi("designer/settings_dialog.ui", self)
        self._connectSignalsSlots()
        self.setDefaultValues()


                                                                # Публичные функции окна


    # Функция установки дефолтных значений
    # settingsList - список значений настроек
    def setDefaultValues(self, settingsList=[]):
        pass


                                                                # Приватные функции окна

    
    # Не соединяю слоты у двух кнопок, потому что ОК/Отмена в контроллере описывается (т.к. там взаимодействие между окнами)
    # Другие элементы здесь будут соединяться по мере добавления настроек
    def _connectSignalsSlots(self):
        pass


                                                                # Слоты для функционала в пределах окна


    # Слот для выбора папки для проекта по умолчанию
    def _selectProjectFolder(self):
        path = QFileDialog.getExistingDirectory(self, 'Выберите папку для проектов по умолчанию', directory=self.ui.lineEditProjectFolder.text())
        path = os.path.normpath(path)
        self.ui.lineEditProjectFolder.setText(path)


    # Слот для изменения текста в полях ввода
    def _updateProjectFilesFolderLabel(self):
        pass
        #projectFolder = self.ui.lineEditProjectFolder.text()
        #projectName = self.ui.lineEditProjectName.text()

        #if self.ui.checkBoxCreateSubfolder.isChecked():
        #    self.ui.labelProjectFilesFolder.setText(os.path.join(projectFolder, projectName))
        #else:
        #    self.ui.labelProjectFilesFolder.setText(projectFolder)



if __name__ == "__main__":
    app = QApplication(sys.argv)

    # Иконки
    appIcon = QtGui.QIcon()
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(16,16))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(24,24))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(32,32))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(48,48))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(256,256))

    app.setWindowIcon(appIcon)
    
    dial = SettingsDialog()
    dial.show()
    sys.exit(app.exec())