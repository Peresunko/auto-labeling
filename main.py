from PyQt5.QtWidgets import QApplication
from PyQt5.Qt import QCoreApplication
from PyQt5 import QtCore, QtGui

from packages.views.startup_window import StartUpWindow
from packages.views.workspace_window import WorkspaceWindow
from packages.views.resources import startup_window_resources
#from packages.views.new_project_dialog import NewProjectDialog
#from packages.views.settings_dialog import SettingsDialog

from packages.model.global_settings_manager import GlobalSettingsManager
from packages.model.project_manager import ProjectManager

from packages.controllers.workspace_controller import WorkspaceController

import packages.constants as const
import sys


if __name__ == "__main__":    
    app = QApplication(sys.argv)

    # Данные для QSettings
    QCoreApplication.setApplicationName(const.ORGANIZATION_NAME)
    QCoreApplication.setOrganizationDomain(const.ORGANIZATION_DOMAIN)
    QCoreApplication.setApplicationName(const.SETTINGS_VALUE_APPLICATION_NAME)

    # Иконки
    appIcon = QtGui.QIcon()
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(16,16))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(24,24))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(32,32))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(48,48))
    appIcon.addFile(":/resources/icons/app_logo.png", QtCore.QSize(256,256))


    app.setWindowIcon(appIcon)
    
    # Окна и контроллер
    viewStartUp = StartUpWindow()
    viewWorkspace = WorkspaceWindow()
    #dialogNewProject = NewProjectDialog()
    #dialogSettings = SettingsDialog()

    projectManager = ProjectManager()
    globalSettingsManager = GlobalSettingsManager()

    cntrl = WorkspaceController(viewStartUp, viewWorkspace, projectManager, globalSettingsManager)

    sys.exit(app.exec())
